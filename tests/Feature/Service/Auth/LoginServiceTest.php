<?php

namespace Tests\Feature\Service\Auth;

use App\Models\User\User;
use App\Services\Auth\LoginService;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Throwable;

class LoginServiceTest extends TestCase
{
    private LoginService $loginService;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loginService = $this->app->make(LoginService::class);
    }

    /**
     * Test Check And Get User Logged In - Success
     *
     * @return void
     */
    public function testCheckAndGetUserLoggedInSuccess(): void
    {
        $this->assertModelExists(
            $this->loginService->checkAndGetUserLoggedIn(
                makeRequest([
                    'email' => 'admin01@komikutama.com',
                    'password' => 'admin01'
                ])
            )
        );
    }

    /**
     * Test Check And Get User Logged In - Invalid Credentials
     *
     * @return void
     */
    public function testCheckAndGetUserLoggedInInvalidCredentials(): void
    {
        $isInstanceOfException = false;
        $isCodeOfExceptionRight = false;
        $isMessageExceptionSame = false;

        try {
            $this->loginService->checkAndGetUserLoggedIn(
                makeRequest([
                    'email' => 'admin01@komikutama.com',
                    'password' => 'admin02'
                ])
            );
        } catch (Throwable $th) {
            $isInstanceOfException = $th instanceof Exception;
            $isCodeOfExceptionRight = $th->getCode() === Response::HTTP_BAD_REQUEST;
            $isMessageExceptionSame = $th->getMessage() === trans('auth.failed');
        }

        $this->assertTrue($isInstanceOfException && $isCodeOfExceptionRight && $isMessageExceptionSame);
    }

    /**
     * Test Create Auth Personal Access Token - Success
     *
     * @return void
     */
    public function testCreateAuthPersonalAccessTokenSuccess(): void
    {
        $user = $this->loginService->checkAndGetUserLoggedIn(
            makeRequest([
                'email' => 'admin01@komikutama.com',
                'password' => 'admin01'
            ])
        );

        $this->assertModelExists($user)->assertIsString($this->loginService->createAuthPersonalAccessToken($user));
    }

    /**
     * Test Revoke Auth Personal Access Token - Success
     *
     * @return void
     */
    public function testRevokeAuthPersonalAccessTokenSuccess(): void
    {
        $user = (new User())->whereHas('tokens')->first();

        $revokeToken = $this->loginService->revokePersonalAccessTokens(collect([$user->tokens->first()]));

        $this->assertTrue($revokeToken);
    }
}
