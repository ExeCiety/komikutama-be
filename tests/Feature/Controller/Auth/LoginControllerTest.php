<?php

namespace Tests\Feature\Controller\Auth;

use App\Helpers\Http\RouteHelper;
use App\Models\User\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    /**
     * Test Login - Success
     *
     * @return void
     */
    public function testLoginSuccess(): void
    {
        $response = $this->post('/' . RouteHelper::$apiRouteVersionPrefixes['v1']['name'] . '/' . RouteHelper::$routePrefixes['public']['name'] . '/auth/login', [
            'email' => 'admin01@komikutama.com',
            'password' => 'admin01'
        ]);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll('status_code', 'message', 'data')
                ->where('status_code', Response::HTTP_OK)
                ->where('message', trans(trans('formatted_response.message.purpose.login')))
                ->has('data', fn(AssertableJson $json) => $json->has('login', fn(AssertableJson $json) => $json->hasAll('access_token', 'user')
                    ->whereType('access_token', 'string')
                    ->whereType('user', 'array'))
                )
            );
    }

    /**
     * Test Logout - Success
     *
     * @return void
     */
    public function testLogoutSuccess(): void
    {
        $user = (new User())->first();
        $response = $this->post('/' . RouteHelper::$apiRouteVersionPrefixes['v1']['name'] . '/' . RouteHelper::$routePrefixes['general']['name'] . '/auth/logout', [], [
            'Authorization' => 'Bearer ' . $user->createToken(config('passport.token_names.auth'))->accessToken
        ]);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll('status_code', 'message')
                ->where('status_code', Response::HTTP_OK)
                ->where('message', trans(trans('formatted_response.message.purpose.logout')))
            );
    }
}
