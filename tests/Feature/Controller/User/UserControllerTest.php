<?php

namespace Tests\Feature\Controller\User;

use App\Helpers\Http\RouteHelper;
use App\Models\User\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    /**
     * Test Get User Info Success
     *
     * @return void
     */
    public function testGetUserInfoSuccess(): void
    {
        $user = (new User())->first();
        $response = $this->get('/' . RouteHelper::$apiRouteVersionPrefixes['v1']['name'] . '/' . RouteHelper::$routePrefixes['general']['name'] . '/users/info', [
            'Authorization' => 'Bearer ' . $user->createToken(config('passport.token_names.auth'))->accessToken
        ]);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(fn(AssertableJson $json) => $json->hasAll('status_code', 'message')
                ->where('status_code', Response::HTTP_OK)
                ->where('message', trans(trans('formatted_response.message.purpose.get')))
                ->has('data', fn(AssertableJson $json) => $json->has('user')
                    ->whereType('user', 'array')
                )
            );
    }
}
