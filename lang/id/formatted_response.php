<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Formatted Response
    |--------------------------------------------------------------------------
    */

    'status_code' => [
        'invalid' => 'Format kode status tidak valid'
    ],
    'message' => [
        'invalid' => 'Format pesan tidak valid',
        'purpose' => [
            'get' => 'Data berhasil diperoleh',
            'create' => 'Data berhasil dibuat',
            'update' => 'Data berhasil diperbarui',
            'delete' => 'Data berhasil dihapus',
            'login' => 'Anda telah berhasil login',
            'logout' => 'Anda telah berhasil logout',
            'upload' => 'File berhasil diunggah',
            'export' => 'Data berhasil diekspor',
            'export_queue' => 'Proses ekspor data dimulai',
        ],
        'model_not_found' => 'Data tidak ditemukan',
        'resource_not_found' => 'Api resource tidak ditemukan',
        'method_not_allowed' => 'Metode :method tidak didukung untuk rute ini. Metode yang didukung: :supported_methods',
        'content_type'
    ],
    'purpose' => [
        'invalid' => 'Format tujuan tidak valid'
    ],
];
