<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password anda sudah direset!',
    'sent' => 'Kami sudah mengirim email link reset password kamu!',
    'throttled' => 'Coba beberapa saat lagi.',
    'token' => 'Token reset password salah.',
    'user' => "Kami tidak dapat menemukan user dengan email address tersebut.",

];
