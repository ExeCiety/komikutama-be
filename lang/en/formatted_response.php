<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Formatted Response
    |--------------------------------------------------------------------------
    */

    'status_code' => [
        'invalid'   => 'Invalid status code format'
    ],
    'message' => [
        'invalid'   => 'Invalid message format',
        'purpose'   => [
            'get'           => 'Data was successfully obtained',
            'create'        => 'Data created successfully',
            'update'        => 'Data updated successfully',
            'delete'        => 'Data deleted successfully',
            'login'         => 'You have successfully logged in',
            'logout'        => 'You have successfully logged out',
            'upload'        => 'File uploaded successfully',
            'export'        => 'Data exported successfully',
            'export_queue'  => 'Data export process started',
        ],
        'model_not_found'   => 'Data not found',
        'resource_not_found'   => 'Resource not found',
        'method_not_allowed'    => 'The :method method is not supported for this route. Supported methods: :supported_methods'
    ],
    'purpose'   => [
        'invalid'   => 'Invalid purpose format',
    ],
];
