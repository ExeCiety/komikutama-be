<?php

return [
    'upload' => [
        'size_limit_exceed' => 'Uploaded file size should be less than or equal to :max_size MB',
    ],
];
