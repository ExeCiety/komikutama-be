<?php

namespace App\Jobs\File;

use Buglinjo\LaravelWebp\Facades\Webp;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ConvertWebp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private array $fileData;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $fileData)
    {
        $this->fileData = $fileData;
    }

    /**
     * Convert Image To Webp With Image Intervention
     *
     * @return self
     */
    private function convertToWebpWithImageIntervention(): self
    {
        if (Storage::disk($this->fileData['disk'])->exists($this->fileData['new_file'])) {
            $image = Storage::disk($this->fileData['disk'])->get($this->fileData['new_file']);

            $canvas = Image::make($image)->encode('webp', $this->fileData['quality'] ?? config('laravel-webp.default_quality'));
            $canvas->save(Storage::disk($this->fileData['disk'])->path('') . $this->fileData['new_file'] . ".webp");

            Storage::disk($this->fileData['disk'])->put($this->fileData['new_file'] . ".webp", $canvas->stream());
        }

        return $this;
    }

    /**
     * Convert Image To Webp With laravel-webp
     *
     * @return self
     */
    private function convertWithLaravelWebp(): self
    {
        Webp::make(
            new UploadedFile(
                Storage::disk($this->fileData['disk'])->path($this->fileData['new_file']),
                $this->fileData['new_file'],
                Storage::disk($this->fileData['disk'])->mimeType($this->fileData['new_file'])
            )
        )
            ->save(
                Storage::disk($this->fileData['disk'])->path($this->fileData['new_file'] . ".webp"),
                $this->fileData['quality'] ?? config('laravel-webp.default_quality')
            );

        return $this;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->convertWithLaravelWebp();
    }
}
