<?php

namespace App\Jobs\File;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MoveFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private array $fileInfo;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $fileInfo)
    {
        $this->fileInfo = $fileInfo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        moveFile([
            'file' => $this->fileInfo['value'],
            'new_file' => resolveTempFileName($this->fileInfo['value'], $this->fileInfo['prefix']),
            'prefix' => $this->fileInfo['prefix'],
            'disk' => $this->fileInfo['disk'],
            'previous_disk' => $this->fileInfo['previous_disk'] ?? config('filesystems.disks.temp.name'),
            'webp' => ($this->fileInfo['type'] == "image" && $this->fileInfo['use_webp']) ?? false
        ]);
    }
}
