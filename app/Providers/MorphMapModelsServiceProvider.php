<?php

namespace App\Providers;

use App\Models\Other\File;
use App\Models\Product\Chapter;
use App\Models\Product\Comic;
use App\Models\Product\Genre;
use App\Models\Product\Page;
use App\Models\User\Artist;
use App\Models\User\Author;
use App\Models\User\Role;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class MorphMapModelsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        Relation::morphMap([
            // User
            'users' => User::class,
            'roles' => Role::class,
            'authors' => Author::class,
            'artists' => Artist::class,

            // Product
            'comics' => Comic::class,
            'chapters' => Chapter::class,
            'pages' => Page::class,
            'genres' => Genre::class,

            // Other
            'files' => File::class,
        ]);
    }
}
