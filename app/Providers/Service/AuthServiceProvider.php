<?php

namespace App\Providers\Service;

use App\Services\Auth\Impl\LoginServiceImpl;
use App\Services\Auth\LoginService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public array $singletons = [
        LoginService::class => LoginServiceImpl::class
    ];

    /**
     * Provides
     *
     * @return string[]
     */
    public function provides(): array
    {
        return [
            LoginService::class
        ];
    }
}
