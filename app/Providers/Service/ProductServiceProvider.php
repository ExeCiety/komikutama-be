<?php

namespace App\Providers\Service;

use App\Services\Product\ChapterService;
use App\Services\Product\ComicService;
use App\Services\Product\GenreService;
use App\Services\Product\Impl\ChapterServiceImpl;
use App\Services\Product\Impl\ComicServiceImpl;
use App\Services\Product\Impl\GenreServiceImpl;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public array $singletons = [
        ComicService::class => ComicServiceImpl::class,
        GenreService::class => GenreServiceImpl::class,
        ChapterService::class => ChapterServiceImpl::class
    ];

    /**
     * Provides
     *
     * @return string[]
     */
    public function provides(): array
    {
        return [
            ComicService::class,
            GenreService::class,
            ChapterService::class
        ];
    }
}
