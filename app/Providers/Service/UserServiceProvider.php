<?php

namespace App\Providers\Service;

use App\Services\User\ArtistService;
use App\Services\User\AuthorService;
use App\Services\User\Impl\ArtistServiceImpl;
use App\Services\User\Impl\AuthorServiceImpl;
use App\Services\User\Impl\RoleServiceImpl;
use App\Services\User\Impl\UserServiceImpl;
use App\Services\User\RoleService;
use App\Services\User\UserService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public array $singletons = [
        UserService::class => UserServiceImpl::class,
        RoleService::class => RoleServiceImpl::class,
        AuthorService::class => AuthorServiceImpl::class,
        ArtistService::class => ArtistServiceImpl::class,
    ];

    /**
     * Provides
     *
     * @return string[]
     */
    public function provides(): array
    {
        return [
            UserService::class,
            RoleService::class,
            AuthorService::class,
            ArtistService::class,
        ];
    }
}
