<?php

namespace App\Providers;

use App\Helpers\Http\RouteHelper;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            $this->setApiRoutes();
            $this->setWebRoutes();
        });
    }

    /**
     * Set Api Routes
     *
     * @return void
     */
    private function setApiRoutes(): void
    {
        $v1ApiRouteVersionPrefix = RouteHelper::$apiRouteVersionPrefixes['v1']['name'];

        Route::prefix('/')
            ->middleware(['api'])
            ->group(base_path('routes/api.php'));

        Route::prefix('/' . $v1ApiRouteVersionPrefix . '/' . RouteHelper::$routePrefixes['admin']['name'])
            ->middleware(['api', 'auth:api', 'role:admin'])
            ->group(base_path('routes/api/v1/admin.php'));

        Route::prefix('/' . $v1ApiRouteVersionPrefix . '/' . RouteHelper::$routePrefixes['general']['name'])
            ->middleware(['api', 'auth:api'])
            ->group(base_path('routes/api/v1/general.php'));

        Route::prefix('/' . $v1ApiRouteVersionPrefix . '/' . RouteHelper::$routePrefixes['public']['name'])
            ->middleware(['api'])
            ->group(base_path('routes/api/v1/public.php'));
    }

    /**
     * Set Web Routes
     *
     * @return void
     */
    public function setWebRoutes(): void
    {
        Route::middleware('web')
            ->prefix('/web')
            ->group(base_path('routes/web.php'));
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }
}
