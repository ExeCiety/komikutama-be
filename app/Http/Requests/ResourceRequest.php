<?php

namespace App\Http\Requests;

interface ResourceRequest
{
    /**
     * Get Create Rules
     *
     * @return string[][]
     */
    public function getCreateRules(): array;

    /**
     * Get Update Rules
     *
     * @return string[][]
     */
    public function getUpdateRules(): array;

    /**
     * Get Delete Rules
     *
     * @return string[][]
     */
    public function getDeleteRules(): array;
}
