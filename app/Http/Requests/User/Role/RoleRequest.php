<?php

namespace App\Http\Requests\User\Role;

use App\Http\Requests\ResourceRequest;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class RoleRequest extends FormRequest implements ResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Prepare For Validation
     *
     * @return void
     */
    public function prepareForValidation(): void
    {
        $this->merge([
            'guard_name' => $this->guard_name ?? config('auth.defaults.guard'),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'POST' => $this->getCreateRules(),
            'PUT' => $this->getUpdateRules(),
            'DELETE' => $this->getDeleteRules(),
            default => [],
        };
    }

    /**
     * Get Create Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['name' => "string[]", 'guard_name' => "string[]"])]
    public function getCreateRules(): array
    {
        return [
            'name' => [
                'required', 'string', 'max:255',
            ],
            'guard_name' => [
                'required', 'string', 'in:' . implode(',', array_keys(config('auth.guards'))),
            ]
        ];
    }

    /**
     * Get Update Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['name' => "string[]", 'guard_name' => "string[]"])]
    public function getUpdateRules(): array
    {
        return [
            'name' => [
                'string', 'max:255',
            ],
            'guard_name' => [
                'string', 'in:' . implode(',', array_keys(config('auth.guards'))),
            ]
        ];
    }

    /**
     * Get Delete Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['id' => "string[]", 'id.*' => "string[]"])]
    public function getDeleteRules(): array
    {
        return [
            'id' => [
                'required', 'array'
            ],
            'id.*' => [
                'required', 'uuid', 'exists:roles,id',
            ]
        ];
    }
}
