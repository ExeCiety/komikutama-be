<?php

namespace App\Http\Requests\User\User;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class UserChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['old_password' => "string[]", 'new_password' => "string[]"])]
    public function rules(): array
    {
        return [
            'old_password' => [
                'required', 'string',
            ],
            'new_password' => [
                'required', 'string', 'confirmed', 'different:old_password',
            ]
        ];
    }
}
