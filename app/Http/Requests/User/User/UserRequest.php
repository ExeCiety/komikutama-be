<?php

namespace App\Http\Requests\User\User;

use App\Helpers\Model\User\UserRoleHelper;
use App\Http\Requests\ResourceRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use JetBrains\PhpStorm\ArrayShape;

class UserRequest extends FormRequest implements ResourceRequest
{
    private bool $fromSelf;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Prepare For Validation
     *
     * @return void
     */
    public function prepareForValidation(): void
    {
        $this->fromSelf = last($this->segments()) === 'self';

        $this->merge([
            'email_verified_at' => $this->method() === 'POST' ? Carbon::now()->format('Y-m-d H:i:s') : null,
            'password' => !$this->fromSelf ? $this->input('password') : null
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'POST' => $this->getCreateRules(),
            'PUT' => $this->getUpdateRules(),
            'DELETE' => $this->getDeleteRules(),
            default => [],
        };
    }

    /**
     * Get Create Rules
     *
     * @return string[]
     */
    #[ArrayShape(['name' => "string[]", 'email' => "string[]", 'password' => "string[]", 'roles' => "string[]", 'roles.*' => "string[]"])]
    public function getCreateRules(): array
    {
        return [
            'name' => [
                'required', 'string', 'max:255'
            ],
            'email' => [
                'required', 'string', 'email', 'max:255', 'unique:users,email'
            ],
            'password' => [
                'required', 'string', 'min:6', 'confirmed'
            ],

            'roles' => [
                'required', 'array'
            ],
            'roles.*' => [
                'in:' . implode(',', UserRoleHelper::getAllRoleNames())
            ]
        ];
    }

    /**
     * Get Update Rules
     *
     * @return string[]
     */
    #[ArrayShape(['name' => "string[]", 'email' => "string[]", 'password' => "string[]", 'roles' => "string[]", 'roles.*' => "string[]"])]
    public function getUpdateRules(): array
    {
        $rules = [
            'name' => [
                'string', 'max:255'
            ],
            'email' => [
                'string', 'email', 'max:255', 'unique:users,email,' . ($this->fromSelf ? $this->user()->id : $this->route('param'))
            ],

            'roles' => [
                'array'
            ],
            'roles.*' => [
                'in:' . implode(',', UserRoleHelper::getAllRoleNames())
            ]
        ];

        if (!$this->fromSelf) {
            $rules['password'] = [
                'nullable', 'string', 'min:6', 'confirmed'
            ];
        }

        return $rules;
    }

    /**
     * Get Delete Rules
     *
     * @return string[]
     */
    #[ArrayShape(['id' => "string[]", 'id.*' => "string[]"])]
    public function getDeleteRules(): array
    {
        return [
            'id' => [
                'required', 'array'
            ],
            'id.*' => [
                'required', 'uuid', 'exists:users,id'
            ],
        ];
    }
}
