<?php

namespace App\Http\Requests\User\Artist;

use App\Helpers\Model\ModelHelper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

class ArtistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Prepare For Validation
     *
     * @return void
     */
    public function prepareForValidation(): void
    {
        $this->merge([
            'slug' => Str::slug($this->input('name') . '-' . Str::random(ModelHelper::$defaultSlugLength))
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'POST' => $this->getCreateRules(),
            'PUT' => $this->getUpdateRules(),
            'DELETE' => $this->getDeleteRules(),
            default => [],
        };
    }

    /**
     * Get Create Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['user_id' => "string[]", 'slug' => "string[]", 'name' => "string[]", 'origin_name' => "string[]", 'biography' => "string[]"])]
    public function getCreateRules(): array
    {
        return [
            'user_id' => [
                'nullable', 'exists:users,id'
            ],
            'slug' => [
                'required', 'string'
            ],
            'name' => [
                'required', 'string', 'max:244'
            ],
            'origin_name' => [
                'nullable', 'string', 'max:255'
            ],
            'biography' => [
                'nullable', 'string', 'max:65535'
            ]
        ];
    }

    /**
     * Get Update Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['user_id' => "string[]", 'slug' => "string[]", 'name' => "string[]", 'origin_name' => "string[]", 'biography' => "string[]"])]
    public function getUpdateRules(): array
    {
        return [
            'user_id' => [
                'nullable', 'exists:users,id'
            ],
            'slug' => [
                'string', 'max:255'
            ],
            'name' => [
                'string', 'max:244'
            ],
            'origin_name' => [
                'string', 'max:255'
            ],
            'biography' => [
                'string', 'max:65535'
            ]
        ];
    }

    /**
     * Get Delete Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['id' => "string[]", 'id.*' => "string[]"])]
    public function getDeleteRules(): array
    {
        return [
            'id' => [
                'required', 'array'
            ],
            'id.*' => [
                'required', 'uuid', 'exists:artists,id'
            ],
        ];
    }
}
