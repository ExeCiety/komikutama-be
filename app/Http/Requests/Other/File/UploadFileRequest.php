<?php

namespace App\Http\Requests\Other\File;

use App\Helpers\Model\Other\FileHelper;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class UploadFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['file' => "string", 'file.*' => "array"])]
    public function rules(): array
    {
        return [
            'file' => 'required|array',
            'file.*' => [
                'mimes:jpg,jpeg,bmp,png,gif,svg,webp,pdf,doc',
                function ($attribute, $value, $fail) {
                    // SET MAX UPLOAD MAX SIZE TO 8MB
                    if ($value->getSize() >= FileHelper::$maxUploadSize) {
                        $fail(
                            trans('file.max_size_exceed', [
                                'max_size' => FileHelper::$maxUploadSize / 1024 / 1024
                            ])
                        );
                    }
                }
            ]
        ];
    }
}
