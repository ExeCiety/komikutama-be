<?php

namespace App\Http\Requests\Product\Genre;

use App\Helpers\Model\ModelHelper;
use App\Http\Requests\ResourceRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\ArrayShape;

class GenreRequest extends FormRequest implements ResourceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function prepareForValidation()
    {
        $this->merge([
            'slug' => Str::slug($this->input('name') . '-' . Str::random(ModelHelper::$defaultSlugLength))
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'POST' => $this->getCreateRules(),
            'PUT' => $this->getUpdateRules(),
            'DELETE' => $this->getDeleteRules(),
            default => [],
        };
    }

    /**
     * Get Create Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['name' => "string[]", 'slug' => "string[]"])]
    public function getCreateRules(): array
    {
        return [
            'name' => [
                'required', 'string', 'max:255',
            ],
            'slug' => [
                'required', 'string', 'unique:genres,slug',
            ],
        ];
    }

    /**
     * Get Update Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['name' => "string[]", 'slug' => "string[]"])]
    public function getUpdateRules(): array
    {
        return [
            'name' => [
                'string', 'max:255',
            ],
            'slug' => [
                'string', 'unique:genres,slug', 'unique:genres,slug,' . $this->route('param')
            ],
        ];
    }

    /**
     * Get Delete Rules
     *
     * @return string[][]
     */
    #[ArrayShape(['id' => "string[]", 'id.*' => "string[]"])]
    public function getDeleteRules(): array
    {
        return [
            'id' => [
                'required', 'array'
            ],
            'id.*' => [
                'required', 'uuid', 'exists:genres,id',
            ]
        ];
    }
}
