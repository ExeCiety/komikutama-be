<?php

namespace App\Http\Requests\Product\Chapter;

use App\Helpers\Model\ModelHelper;
use App\Helpers\Model\User\UserRoleHelper;
use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ChapterRequest extends FormRequest
{
    private User|null $userAuth;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $authorized = false;

        if ($this->method() === 'POST' || $this->method() === 'PUT' || $this->method() === 'DELETE') {
            $authorized = (bool)$this->userAuth;
        }

        return $authorized;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->userAuth = $this->user();

        $this->merge([
            'publisher_id' => $this->input('publisher_id') ?? $this->userAuth?->id,
            'slug' => Str::slug($this->input('title') . '-' . Str::random(ModelHelper::$defaultSlugLength))
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'POST' => $this->getCreateRules(),
            'PUT' => $this->getUpdateRules(),
            'DELETE' => $this->getDeleteRules(),
            default => []
        };
    }

    /**
     * Get Create Rules
     *
     * @return string[][]
     */
    public function getCreateRules(): array
    {
        return [
            'publisher_id' => [
                'uuid', 'exists:users,id', Rule::requiredIf(function () {
                    return !$this->userAuth?->hasRole([UserRoleHelper::$userRoles['admin']['name']]);
                })
            ],
            'ownerable_type' => [
                'required', 'string'
            ],
            'ownerable_id' => [
                'required', 'uuid', "exists:{$this->input('ownerable_type')},id"
            ],
            'title' => [
                'required', 'string', 'max:255'
            ],
            'slug' => [
                'required', 'string', 'max:255'
            ],
            'sequence' => [
                'integer', 'min:1'
            ],

            'medias' => [
                'required', 'array'
            ],
            'medias.*.name' => [
                'required', 'string', 'in:cover'
            ],
            'medias.*.value' => [
                'required', 'string'
            ],
            'medias.*.type' => [
                'required', 'string', 'in:file,image,youtube_video'
            ],

            'pages' => [
                'array'
            ],
            'pages.*.content' => [
                'nullable', 'string'
            ],
            'pages.*.sequence' => [
                'required', 'integer', 'min:1'
            ],
            'pages.*.medias' => [
                'required', 'array'
            ],
            'pages.*.medias.*.name' => [
                'required', 'string', 'in:page'
            ],
            'pages.*.medias.*.value' => [
                'required', 'string'
            ],
            'pages.*.medias.*.type' => [
                'required', 'string', 'in:file,image,youtube_video'
            ],
        ];
    }

    /**
     * Get Update Rules
     *
     * @return string[][]
     */
    public function getUpdateRules(): array
    {
        return [
            'publisher_id' => [
                'uuid', 'exists:users,id', Rule::requiredIf(function () {
                    return !$this->userAuth?->hasRole([UserRoleHelper::$userRoles['admin']['name']]);
                })
            ],
            'ownerable_type' => [
                'string'
            ],
            'ownerable_id' => [
                'uuid', "exists:{$this->input('ownerable_type')},id"
            ],
            'title' => [
                'string', 'max:255'
            ],
            'slug' => [
                'string', 'max:255'
            ],
            'sequence' => [
                'integer', 'min:1'
            ],

            'medias' => [
                'array'
            ],
            'medias.*.name' => [
                'required', 'string', 'in:cover'
            ],
            'medias.*.value' => [
                'required', 'string'
            ],
            'medias.*.type' => [
                'required', 'string', 'in:file,image,youtube_video'
            ],

            'pages' => [
                'array'
            ],
            'pages.*.content' => [
                'nullable', 'string'
            ],
            'pages.*.sequence' => [
                'integer', 'min:1'
            ],
            'pages.*.medias' => [
                'array'
            ],
            'pages.*.medias.*.name' => [
                'required', 'string', 'in:page'
            ],
            'pages.*.medias.*.value' => [
                'required', 'string'
            ],
            'pages.*.medias.*.type' => [
                'required', 'string', 'in:file,image,youtube_video'
            ],
        ];
    }

    /**
     * Get Delete Rules
     *
     * @return string[][]
     */
    public function getDeleteRules(): array
    {
        return [
            'id' => [
                'required', 'array'
            ],
            'id.*' => [
                'required', 'uuid', 'exists:chapters,id'
            ]
        ];
    }
}
