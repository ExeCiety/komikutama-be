<?php

namespace App\Http\Requests\Product\Comic;

use App\Helpers\Model\ModelHelper;
use App\Helpers\Model\Product\ComicHelper;
use App\Helpers\Model\User\UserRoleHelper;
use App\Http\Requests\ResourceRequest;
use App\Models\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

class ComicRequest extends FormRequest implements ResourceRequest
{
    private User|null $userAuth;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $authorized = false;

        if ($this->method() === 'POST' || $this->method() === 'PUT' || $this->method() === 'DELETE') {
            $authorized = (bool)$this->userAuth;
        }

        return $authorized;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation(): void
    {
        $this->userAuth = $this->user();

        $this->merge([
            'publisher_id' => $this->input('publisher_id') ?? $this->userAuth?->id,
            'slug' => Str::slug($this->input('title') . '-' . Str::random(ModelHelper::$defaultSlugLength))
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return match ($this->method()) {
            'POST' => $this->getCreateRules(),
            'PUT' => $this->getUpdateRules(),
            'DELETE' => $this->getDeleteRules(),
            default => [],
        };
    }

    /**
     * Get Create Rules
     *
     * @return string[][]
     */
    public function getCreateRules(): array
    {
        return [
            'publisher_id' => [
                'uuid', 'exists:users,id', Rule::requiredIf(function () {
                    return !$this->userAuth?->hasRole([UserRoleHelper::$userRoles['admin']['name']]);
                })
            ],
            'author_id' => [
                'required', 'array'
            ],
            'author_id.*' => [
                'uuid', 'exists:authors,id'
            ],
            'artist_id' => [
                'required', 'array'
            ],
            'artist_id.*' => [
                'uuid', 'exists:artists,id'
            ],
            'slug' => [
                'required', 'string', 'max:255'
            ],
            'title' => [
                'required', 'string', 'max:244'
            ],
            'alternative_title' => [
                'required', 'string', 'max:255'
            ],
            'synopsis' => [
                'required', 'string', 'max:65535'
            ],
            'year_released_at' => [
                'required', 'date_format:Y'
            ],
            'rank' => [
                'nullable', 'integer'
            ],
            'rating' => [
                'nullable', 'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'status' => [
                'required', 'string', 'in:' . implode(',', ComicHelper::$comicStatuses)
            ],
            'type' => [
                'required', 'string', 'in:' . implode(',', ComicHelper::$comicTypes)
            ],

            'medias' => [
                'required', 'array'
            ],
            'medias.*.name' => [
                'required', 'string', 'in:cover'
            ],
            'medias.*.value' => [
                'required', 'string'
            ],
            'medias.*.type' => [
                'in:file,image,youtube_video'
            ],
        ];
    }

    /**
     * @return string[][]
     */
    public function getUpdateRules(): array
    {
        return [
            'publisher_id' => [
                'uuid', 'exists:users,id'
            ],
            'author_id' => [
                'array'
            ],
            'author_id.*' => [
                'uuid', 'exists:authors,id'
            ],
            'artist_id' => [
                'array'
            ],
            'artist_id.*' => [
                'uuid', 'exists:artists,id'
            ],
            'slug' => [
                'string', 'max:255'
            ],
            'title' => [
                'string', 'max:244'
            ],
            'alternative_title' => [
                'string', 'max:255'
            ],
            'synopsis' => [
                'string', 'max:65535'
            ],
            'year_released_at' => [
                'date_format:Y'
            ],
            'rank' => [
                'nullable', 'integer'
            ],
            'rating' => [
                'nullable', 'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'status' => [
                'string', 'in:' . implode(',', ComicHelper::$comicStatuses)
            ],
            'type' => [
                'string', 'in:' . implode(',', ComicHelper::$comicTypes)
            ],

            'medias' => [
                'array'
            ],
            'medias.*.name' => [
                'required', 'string', 'in:cover'
            ],
            'medias.*.value' => [
                'required', 'string'
            ],
            'medias.*.type' => [
                'in:file,image,youtube_video'
            ],
        ];
    }

    /**
     * @return string[][]
     */
    #[ArrayShape(['id' => "string[]", 'id.*' => "string[]"])]
    public function getDeleteRules(): array
    {
        return [
            'id' => [
                'required', 'array'
            ],
            'id.*' => [
                'required', 'uuid', 'exists:comics,id',
            ]
        ];
    }
}
