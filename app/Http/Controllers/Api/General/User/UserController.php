<?php

namespace App\Http\Controllers\Api\General\User;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\General\GeneralController;
use App\Http\Requests\User\User\UserChangePasswordRequest;
use App\Http\Requests\User\User\UserRequest;
use App\Http\Resources\User\User\UserForGetInfoResource;
use App\Services\User\UserService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UserController extends GeneralController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->setRequest()->setUserAuth()->setDataKeys('user');

        $this->userService = $userService;
    }

    /**
     * Get User Logged In
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function getUserLoggedIn(): JsonResponse
    {
        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new UserForGetInfoResource($this->userAuth)
                ])
        );
    }

    /**
     * Update User Logged In
     *
     * @param UserRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function updateUserLoggedIn(UserRequest $request): JsonResponse
    {
        $user = $this->userService->updateUser($request, $this->userAuth);
        if ($request->input('role')) {
            $this->userService->syncUserRoles($user, $request->input('roles'));
        }

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * @param UserChangePasswordRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function changePasswordUserLoggedIn(UserChangePasswordRequest $request): JsonResponse
    {
        $isPasswordCorrect = $this->userService->checkUserPasswordIsCorrect($this->userAuth, $request->input('old_password'));

        if (!$isPasswordCorrect) {
            return $this->makeJsonResponse(
                $this->makeResponsePayload()
                    ->setStatusCode(ResponseAlias::HTTP_UNPROCESSABLE_ENTITY)
                    ->setMessage(trans('user.change_password.old_password_incorrect'))
                    ->setData([
                        'old_password' => [
                            'message' => trans('user.change_password.old_password_incorrect')
                        ]
                    ])
            );
        }

        $this->userService->updateUser(makeRequest([
            'password' => $this->request->input('new_password')
        ]), $this->userAuth);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }
}
