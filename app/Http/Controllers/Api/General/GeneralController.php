<?php

namespace App\Http\Controllers\Api\General;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class GeneralController extends ApiController
{
    /**
     * Home
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function home(): JsonResponse
    {
        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessage('Welcome to Komikutama API General Routes')
                ->setData([
                    "title" => "Komikutama API General Routes",
                    "description" => "API General Routes for Komikutama",
                ])
        );
    }
}
