<?php

namespace App\Http\Controllers\Api\General\Other;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\General\GeneralController;
use App\Http\Requests\Other\File\UploadFileRequest;
use App\Traits\Storage\UploadFile;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class FileController extends GeneralController
{
    use UploadFile;

    /**
     * Upload File From Request
     *
     * @param UploadFileRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function upload(UploadFileRequest $request): JsonResponse
    {
        $this->setRequest($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('upload')
                ->setData([
                    'files' => $this->uploadRequestFilesToStorage($request)
                ])
        );
    }
}
