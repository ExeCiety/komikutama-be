<?php

namespace App\Http\Controllers\Api\General\Auth;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\General\GeneralController;
use App\Services\Auth\LoginService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class LoginController extends GeneralController
{
    private LoginService $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->setRequest()->setUserAuth();

        $this->loginService = $loginService;
    }

    /**
     * Logout
     *
     * @param Request $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function logout(Request $request): JsonResponse
    {
        $this->loginService->revokePersonalAccessTokens(collect([$this->userAuth->token()]));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('logout')
        );
    }
}
