<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\User\User\UserRequest;
use App\Http\Resources\User\User\UserForAdminResource;
use App\Http\Resources\User\User\UserForListAdminCollection;
use App\Services\User\UserService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UserController extends AdminController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->setRequest()->setDataKeys('user');

        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(): JsonResponse
    {
        $users = $this->userService->getUsers($this->request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new UserForListAdminCollection($users)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(string $param): JsonResponse
    {
        $user = $this->userService->getUser($this->request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new UserForAdminResource($user)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(UserRequest $request): JsonResponse
    {
        $user = $this->userService->createUser($request);
        $this->userService->syncUserRoles($user, $request->input('roles'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(UserRequest $request, string $param): JsonResponse
    {
        $user = $this->userService->getUser($request->merge(['find_or_fail' => true]), $param);
        $user = $this->userService->updateUser($request, $user);

        if ($request->input('roles') !== null) {
            $this->userService->syncUserRoles($user, $request->input('roles'));
        }

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param UserRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(UserRequest $request): JsonResponse
    {
        $users = $this->userService->deleteAssignedRoles($request->input('id'));
        $this->userService->deleteUsers($request, $users->pluck('id')->toArray());

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('delete')
        );
    }
}
