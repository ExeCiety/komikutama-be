<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\User\Artist\ArtistRequest;
use App\Http\Resources\User\Artist\ArtistForAdminResource;
use App\Http\Resources\User\Artist\ArtistForListAdminCollection;
use App\Services\User\ArtistService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ArtistController extends AdminController
{
    private ArtistService $artistService;

    public function __construct(ArtistService $artistService)
    {
        $this->setRequest()->setDataKeys('artist');

        $this->artistService = $artistService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(): JsonResponse
    {
        $artists = $this->artistService->getArtists($this->request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new ArtistForListAdminCollection($artists)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(string $param): JsonResponse
    {
        $artist = $this->artistService->getArtist($this->request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new ArtistForAdminResource($artist)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArtistRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(ArtistRequest $request): JsonResponse
    {
        $this->artistService->createArtist($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ArtistRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(ArtistRequest $request, string $param): JsonResponse
    {
        $artist = $this->artistService->getArtist($this->request->merge(['find_or_fail' => true]), $param);
        $this->artistService->updateArtist($request, $artist);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ArtistRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(ArtistRequest $request): JsonResponse
    {
        $this->artistService->deleteArtists($request, $request->input('id'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('delete')
        );
    }
}
