<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\User\Author\AuthorRequest;
use App\Http\Resources\User\Author\AuthorForAdminResource;
use App\Http\Resources\User\Author\AuthorForListAdminCollection;
use App\Services\User\AuthorService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AuthorController extends AdminController
{
    private AuthorService $authorService;

    public function __construct(AuthorService $authorService)
    {
        $this->setRequest()->setDataKeys('author');

        $this->authorService = $authorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(): JsonResponse
    {
        $authors = $this->authorService->getAuthors($this->request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new AuthorForListAdminCollection($authors)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(string $param): JsonResponse
    {
        $author = $this->authorService->getAuthor($this->request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new AuthorForAdminResource($author)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AuthorRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(AuthorRequest $request): JsonResponse
    {
        $this->authorService->createAuthor($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AuthorRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(AuthorRequest $request, string $param): JsonResponse
    {
        $author = $this->authorService->getAuthor($this->request, $param);
        $this->authorService->updateAuthor($request, $author);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AuthorRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(AuthorRequest $request): JsonResponse
    {
        $this->authorService->deleteAuthors($request, $request->input('id'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('delete')
        );
    }
}
