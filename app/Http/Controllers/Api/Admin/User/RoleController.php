<?php

namespace App\Http\Controllers\Api\Admin\User;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\User\Role\RoleRequest;
use App\Http\Resources\User\Role\RoleForAdminResource;
use App\Http\Resources\User\Role\RoleForListAdminCollection;
use App\Services\User\RoleService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class RoleController extends AdminController
{
    private RoleService $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->setRequest()->setUserAuth()->setDataKeys('role');

        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(): JsonResponse
    {
        $users = $this->roleService->getRoles($this->request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new RoleForListAdminCollection($users)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(string $param): JsonResponse
    {
        $role = $this->roleService->getRole($this->request, $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new RoleForAdminResource($role)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(RoleRequest $request): JsonResponse
    {
        $this->roleService->createRole($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(RoleRequest $request, string $param): JsonResponse
    {
        $role = $this->roleService->getRole($this->request, $param);
        $this->roleService->updateRole($request, $role);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param RoleRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(RoleRequest $request): JsonResponse
    {
        $this->roleService->deleteRoles($request, $request->input('id'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('delete')
        );
    }
}
