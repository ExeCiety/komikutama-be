<?php

namespace App\Http\Controllers\Api\Admin;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AdminController extends ApiController
{
    /**
     * Home
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function home(): JsonResponse
    {
        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessage('Welcome to Komikutama API Admin Routes')
                ->setData([
                    "title" => "Komikutama API",
                    "description" => "API Admin Routes for Komikutama",
                ])
        );
    }
}
