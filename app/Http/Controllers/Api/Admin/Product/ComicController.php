<?php

namespace App\Http\Controllers\Api\Admin\Product;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\Product\Comic\ComicRequest;
use App\Http\Resources\Product\Comic\ComicForAdminResource;
use App\Http\Resources\Product\Comic\ComicForListAdminCollection;
use App\Services\Product\ComicService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ComicController extends AdminController
{
    private ComicService $comicService;

    public function __construct(ComicService $comicService)
    {
        $this->setDataKeys('comic')->setUserAuth();

        $this->comicService = $comicService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(Request $request): JsonResponse
    {
        $comics = $this->comicService->getComics($request);

        
        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new ComicForListAdminCollection($comics)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(Request $request, string $param): JsonResponse
    {
        $comic = $this->comicService->findComic($request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new ComicForAdminResource($comic)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ComicRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(ComicRequest $request): JsonResponse
    {
        $this->comicService->createComic($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ComicRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(ComicRequest $request, string $param): JsonResponse
    {
        $comic = $this->comicService->findComic($request->merge(['find_or_fail' => true]), $param);
        $this->comicService->updateComic($request, $comic);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ComicRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(ComicRequest $request): JsonResponse
    {
        $this->comicService->deleteComics($request, $request->input('id'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('delete')
        );
    }
}
