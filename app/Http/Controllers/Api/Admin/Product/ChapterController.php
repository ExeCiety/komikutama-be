<?php

namespace App\Http\Controllers\Api\Admin\Product;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\Product\Chapter\ChapterRequest;
use App\Http\Requests\Product\Comic\ComicRequest;
use App\Http\Resources\Product\Chapter\ChapterForAdminResource;
use App\Http\Resources\Product\Chapter\ChapterForListAdminCollection;
use App\Services\Product\ChapterService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ChapterController extends AdminController
{
    private ChapterService $chapterService;

    public function __construct(ChapterService $chapterService)
    {
        $this->setDataKeys('chapter')->setUserAuth();

        $this->chapterService = $chapterService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(Request $request): JsonResponse
    {
        $chapters = $this->chapterService->getChapters($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(SymfonyResponse::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new ChapterForListAdminCollection($chapters)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(Request $request, string $param): JsonResponse
    {
        $chapter = $this->chapterService->findChapter($request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(SymfonyResponse::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new ChapterForAdminResource($chapter)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ChapterRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(ChapterRequest $request): JsonResponse
    {
        $chapter = $this->chapterService->createChapter($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(SymfonyResponse::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ChapterRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(ChapterRequest $request, string $param): JsonResponse
    {
        $chapter = $this->chapterService->findchapter($request->merge(['find_or_fail' => true]), $param);
        $this->chapterService->updateChapter($request, $chapter);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(SymfonyResponse::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ChapterRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(ChapterRequest $request): JsonResponse
    {
        $this->chapterService->deleteChapters($request, $request->input('id'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(SymfonyResponse::HTTP_OK)
                ->setMessageFromPurpose('delete')
        );
    }
}
