<?php

namespace App\Http\Controllers\Api\Admin\Product;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Requests\Product\Genre\GenreRequest;
use App\Http\Resources\Product\Genre\GenreForAdminResource;
use App\Http\Resources\Product\Genre\GenreForListAdminCollection;
use App\Services\Product\GenreService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class GenreController extends AdminController
{
    private GenreService $genreService;

    public function __construct(GenreService $genreService)
    {
        $this->setRequest()->setDataKeys('genre');

        $this->genreService = $genreService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(): JsonResponse
    {
        $genres = $this->genreService->getGenres($this->request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new GenreForListAdminCollection($genres)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(string $param): JsonResponse
    {
        $genre = $this->genreService->getGenre($this->request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new GenreForAdminResource($genre)
                ])
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GenreRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function store(GenreRequest $request): JsonResponse
    {
        $this->genreService->createGenre($request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_CREATED)
                ->setMessageFromPurpose('create')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GenreRequest $request
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function update(GenreRequest $request, string $param): JsonResponse
    {
        $user = $this->genreService->getGenre(makeRequest(['find_or_fail' => true]), $param);
        $this->genreService->updateGenre($request, $user);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('update')
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param GenreRequest $request
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function destroy(GenreRequest $request): JsonResponse
    {
        $this->genreService->deleteGenres($request, $request->input('id'));

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('delete')
        );
    }
}
