<?php

namespace App\Http\Controllers\Api\Public;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class PublicController extends ApiController
{
    /**
     * Home
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function home(): JsonResponse
    {
        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessage('Welcome to Komikutama API Public Routes')
                ->setData([
                    "title" => "Komikutama API Public Routes",
                    "description" => "API Public Routes for Komikutama",
                ])
        );
    }
}
