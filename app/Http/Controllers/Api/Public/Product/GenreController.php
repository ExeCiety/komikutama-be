<?php

namespace App\Http\Controllers\Api\Public\Product;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Public\PublicController;
use App\Http\Resources\Product\Genre\GenreForPublicCollection;
use App\Http\Resources\Product\Genre\GenreForPublicResource;
use App\Services\Product\GenreService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class GenreController extends PublicController
{
    private GenreService $genreService;

    public function __construct(GenreService $genreService)
    {
        $this->setRequest()->setDataKeys('genre');

        $this->genreService = $genreService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function index(): JsonResponse
    {
        $genres = $this->genreService->getGenres($this->request);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKeys" => new GenreForPublicCollection($genres)
                ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return JsonResponse
     * @throws FormattedResponseException
     */
    public function show(string $param): JsonResponse
    {
        $genre = $this->genreService->getGenre($this->request->merge(['find_or_fail' => true]), $param);

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('get')
                ->setData([
                    "$this->dataKey" => new GenreForPublicResource($genre)
                ])
        );
    }
}
