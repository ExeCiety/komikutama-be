<?php

namespace App\Http\Controllers\Api\Public\Auth;

use App\Exceptions\Http\FormattedResponseException;
use App\Http\Controllers\Api\Public\PublicController;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\User\User\UserForLoginResource;
use App\Services\Auth\LoginService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Throwable;

class LoginController extends PublicController
{
    private LoginService $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->setDataKeys('login')->setUserAuth();
        
        $this->loginService = $loginService;
    }

    /**
     * Login
     *
     * @throws FormattedResponseException
     */
    public function login(LoginRequest $request): JsonResponse
    {
        try {
            $userLoggedIn = $this->loginService->checkAndGetUserLoggedIn($request);
            $accessToken = $this->loginService->createAuthPersonalAccessToken($userLoggedIn);
        } catch (Throwable $th) {
            return $this->returnCatchThrowableToJsonResponse($th);
        }

        return $this->makeJsonResponse(
            $this->makeResponsePayload()
                ->setStatusCode(ResponseAlias::HTTP_OK)
                ->setMessageFromPurpose('login')
                ->setData([
                    "$this->dataKey" => [
                        'access_token' => $accessToken,
                        'user' => new UserForLoginResource($userLoggedIn)
                    ]
                ])
        );
    }
}
