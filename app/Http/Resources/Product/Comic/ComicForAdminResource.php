<?php

namespace App\Http\Resources\Product\Comic;

use App\Http\Resources\Other\File\FileForDefaultCollection;
use App\Http\Resources\User\Artist\ArtistForListAdminCollection;
use App\Http\Resources\User\Author\AuthorForListAdminCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property string $id
 * @property string $slug
 * @property string $title
 * @property string $alternative_title
 * @property string $synopsis
 * @property string $year_released_at
 * @property string $status
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property array $authors
 * @property array $artists
 * @property array $files
 */
class ComicForAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'alternative_title' => $this->alternative_title,
            'synopsis' => $this->synopsis,
            'year_released_at' => strval($this->year_released_at),
            'status' => $this->status,
            'type' => $this->type,
            'created_at' => convertDateToFormat($this->created_at),
            'updated_at' => convertDateToFormat($this->updated_at),

            'authors' => new AuthorForListAdminCollection($this->authors),
            'artists' => new ArtistForListAdminCollection($this->artists),
            'files' => new FileForDefaultCollection($this->files)
        ];
    }
}
