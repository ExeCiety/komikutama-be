<?php

namespace App\Http\Resources\Product\Chapter;

use App\Http\Resources\Other\File\FileForDefaultCollection;
use App\Http\Resources\Product\Page\PageForListAdminCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property string $id
 * @property string $slug
 * @property string $title
 * @property int $sequence
 * @property string $created_at
 * @property string $updated_at
 */
class ChapterForAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'ownerable_type' => $this->ownerable_type,
            'ownerable_id' => $this->ownerable_id,
            'slug' => $this->slug,
            'title' => $this->title,
            'sequence' => $this->sequence,
            'created_at' => convertDateToFormat($this->created_at),
            'updated_at' => convertDateToFormat($this->updated_at),

            'files' => new FileForDefaultCollection($this->files),
            'pages' => new PageForListAdminCollection($this->pages)
        ];
    }
}
