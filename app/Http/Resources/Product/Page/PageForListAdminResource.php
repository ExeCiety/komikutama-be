<?php

namespace App\Http\Resources\Product\Page;

use App\Http\Resources\Other\File\FileForDefaultCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property string $id
 * @property string $chapter_id
 * @property string $content
 * @property int $sequence
 * @propwerty string $created_at
 * @property string $updated_at
 */
class PageForListAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'chapter_id' => $this->chapter_id,
            'content' => $this->content,
            'sequence' => $this->sequence,
            'created_at' => convertDateToFormat($this->created_at),
            'updated_at' => convertDateToFormat($this->updated_at),

            'files' => new FileForDefaultCollection($this->files)
        ];
    }
}
