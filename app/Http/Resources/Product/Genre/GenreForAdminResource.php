<?php

namespace App\Http\Resources\Product\Genre;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property string $id
 * @property string $slug
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class GenreForAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ArrayShape(['id' => "string", 'slug' => "string", 'name' => "string", 'created_at' => "null|string", 'updated_at' => "null|string"])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'created_at' => convertDateToFormat($this->created_at),
            'updated_at' => convertDateToFormat($this->updated_at),
        ];
    }
}
