<?php

namespace App\Http\Resources\Other\File;

use App\Helpers\Model\Other\FileHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property string $id
 * @property string $name
 * @property string $filename
 * @property string $value
 * @property string $variant
 * @property string $type
 * @property string $fileable_id
 * @property string $fileable_type
 */
class FileForDefaultResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ArrayShape(['id' => "string", 'name' => "string", 'filename' => "string", 'value' => "string", 'variant' => "string", 'type' => "string", 'fileable_id' => "string", 'fileable_type' => "string"])]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'filename' => $this->value,
            'value' => FileHelper::generateUrlValueModelFile($this->resource),
            'variant' => $this->variant,
            'type' => $this->type,
            'fileable_id' => $this->fileable_id,
            'fileable_type' => $this->fileable_type,
        ];
    }
}
