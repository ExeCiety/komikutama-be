<?php

namespace App\Http\Resources\User\User;

use App\Http\Resources\User\Role\RoleForListAdminCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property string $id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $created_at
 * @property string $updated_at
 * @property array $roles
 */
class UserForListAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[
        ArrayShape([
            'id' => "string",
            'name' => "string",
            'email' => "string",
            'email_verified_at' => "null|string",
            'created_at' => "null|string",
            'updated_at' => "null|string",
            'roles' => "\App\Http\Resources\User\Role\RoleForLoginCollection"
        ])
    ]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'email_verified_at' => convertDateToFormat($this->email_verified_at),
            'created_at' => convertDateToFormat($this->created_at),
            'updated_at' => convertDateToFormat($this->updated_at),

            'roles' => new RoleForListAdminCollection($this->roles),
        ];
    }
}
