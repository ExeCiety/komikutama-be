<?php

namespace App\Http\Resources\User\Author;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AuthorForAdminCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function toArray($request): Collection|LengthAwarePaginator
    {
        return $this->resource;
    }
}
