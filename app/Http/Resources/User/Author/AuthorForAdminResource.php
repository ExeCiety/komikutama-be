<?php

namespace App\Http\Resources\User\Author;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property string $id
 * @property string $slug
 * @property string $name
 * @property string $origin_name
 * @property string $biography
 * @property string $created_at
 * @property string $updated_at
 */
class AuthorForAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ArrayShape(
        [
            'id' => "string",
            'slug' => "string",
            'name' => "string",
            'origin_name' => "string",
            'biography' => "string",
            'created_at' => "null|string",
            'updated_at' => "null|string"
        ])
    ]
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'origin_name' => $this->origin_name,
            'biography' => $this->biography,
            'created_at' => convertDateToFormat($this->created_at),
            'updated_at' => convertDateToFormat($this->updated_at),
        ];
    }
}
