<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Localization
{
    private array $supportedLanguages = [
        'en', 'id'
    ];

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $local = ($request->hasHeader('accept-language')) ? $request->header('accept-language') : config('app.locale');

        if (!in_array($local, $this->supportedLanguages)) $local = config('app.locale');

        app()->setLocale($local);

        return $next($request);
    }
}
