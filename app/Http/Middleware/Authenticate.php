<?php

namespace App\Http\Middleware;

use App\Exceptions\Http\FormattedResponseException;
use App\Traits\Http\RequestResponse;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Authenticate extends Middleware
{
    use RequestResponse;

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     * @return JsonResponse|null
     * @throws FormattedResponseException
     */
    protected function redirectTo($request): JsonResponse|null
    {
        if (!$request->expectsJson()) {
            return $this->makeJsonResponse(
                $this->makeResponsePayload()
                    ->setStatusCode(Response::HTTP_UNAUTHORIZED)
                    ->setMessage(Response::$statusTexts[Response::HTTP_UNAUTHORIZED])
            );
        }

        return null;
    }
}
