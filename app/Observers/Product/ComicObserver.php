<?php

namespace App\Observers\Product;

use App\Helpers\Model\Other\FileHelper;
use App\Helpers\Model\Product\ComicHelper;
use App\Models\Product\Comic;
use App\Traits\Storage\FileModelRelation;

class ComicObserver
{
    use FileModelRelation;

    /**
     * Handle the Comic "deleting" event.
     *
     * @param Comic $comic
     * @return void
     * @throws \Exception
     */
    public function deleting(Comic $comic): void
    {
        $comic->artists()->detach();
        $comic->authors()->detach();

        $this->initModelSyncFiles($comic, [
            'file_prefix' => ComicHelper::$filePrefix,
            'file_variants' => FileHelper::getImageVariantNames(ComicHelper::$fileVariants),
            'use_webp' => ComicHelper::$useWebp
        ])
            ->syncModelFiles([])
            ->syncMoveModelFiles();
    }
}
