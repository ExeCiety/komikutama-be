<?php

namespace App\Services\User;

use App\Models\User\Artist;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface ArtistService
{
    public function getArtists(Request $request): Collection|LengthAwarePaginator;

    public function getArtist(Request $request, ?string $param = null): ?Artist;

    public function createArtist(Request $request): Model|Artist;

    public function updateArtist(Request $request, Artist $artist): Artist;

    public function deleteArtists(Request $request, array $artistIds): int;
}
