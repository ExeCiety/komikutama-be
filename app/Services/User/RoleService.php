<?php

namespace App\Services\User;

use App\Models\User\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface RoleService
{
    public function getRoles(Request $request): Collection|LengthAwarePaginator;

    public function getRole(Request $request, ?string $param = null): ?Role;

    public function createRole(Request $request): Model|Role;

    public function updateRole(Request $request, Role $role): Role;

    public function deleteRoles(Request $request, array $roleIds): int;
}
