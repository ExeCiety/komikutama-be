<?php

namespace App\Services\User;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface UserService
{
    public function getUsers(Request $request): Collection|LengthAwarePaginator;

    public function getUser(Request $request, ?string $param = null): ?User;

    public function checkUserPasswordIsCorrect(User $user, string $password): bool;

    public function createUser(Request $request): User;

    public function updateUser(Request $request, User $user): User;

    public function syncUserRoles(User $user, array $roleNames): User;

    public function deleteUsers(Request $request, array $userIds): int;

    public function deleteAssignedRoles(array $userIds): Collection;
}
