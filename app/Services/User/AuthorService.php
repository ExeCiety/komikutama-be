<?php

namespace App\Services\User;

use App\Models\User\Author;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface AuthorService
{
    public function getAuthors(Request $request): Collection|LengthAwarePaginator;

    public function getAuthor(Request $request, ?string $param = null): ?Author;

    public function createAuthor(Request $request): Model|Author;

    public function updateAuthor(Request $request, Author $author): Author;

    public function deleteAuthors(Request $request, array $authorIds): int;
}
