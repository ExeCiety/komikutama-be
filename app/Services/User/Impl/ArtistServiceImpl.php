<?php

namespace App\Services\User\Impl;

use App\Models\User\Artist;
use App\Services\User\ArtistService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ArtistServiceImpl implements ArtistService
{
    private Artist $artistModel;

    public function __construct(Artist $artist)
    {
        $this->artistModel = $artist;
    }

    /**
     * Get Artists
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getArtists(Request $request): Collection|LengthAwarePaginator
    {
        $artists = $this->artistModel->search($request->input('search'));

        if (!isPaginated($request)) return $artists->get();

        return $artists->paginate($request->perPage ?? null);
    }

    /**
     * Get Artist
     *
     * @param Request $request
     * @param string|null $param
     * @return Artist|null
     */
    public function getArtist(Request $request, ?string $param = null): ?Artist
    {
        $artist = $this->artistModel;

        if ($param) {
            $artist = $artist->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%")
                    ->orWhere('slug', '=', $param);
            });
        }

        return $request->input('find_or_fail') ? $artist->firstOrFail() : $artist->first();
    }

    /**
     * Create Artist
     *
     * @param Request $request
     * @return Model|Artist
     */
    public function createArtist(Request $request): Model|Artist
    {
        return $this->artistModel->create(
            array_filter([
                'id' => $request->input('id'),
                'user_id' => $request->input('user_id'),
                'slug' => $request->input('slug'),
                'name' => $request->input('name'),
                'origin_name' => $request->input('origin_name'),
                'biography' => $request->input('biography'),
            ], customArrayFilter())
        );
    }

    /**
     * Update Artist
     *
     * @param Request $request
     * @param Artist $artist
     * @return Artist
     */
    public function updateArtist(Request $request, Artist $artist): Artist
    {
        $artist->update(
            array_filter([
                'user_id' => $request->input('user_id'),
                'slug' => $request->input('slug'),
                'name' => $request->input('name'),
                'origin_name' => $request->input('origin_name'),
                'biography' => $request->input('biography'),
            ], customArrayFilter())
        );

        return $artist->refresh();
    }

    /**
     * Delete Artists
     *
     * @param Request $request
     * @param array $artistIds
     * @return int
     */
    public function deleteArtists(Request $request, array $artistIds): int
    {
        return $this->artistModel->destroy($artistIds);
    }
}
