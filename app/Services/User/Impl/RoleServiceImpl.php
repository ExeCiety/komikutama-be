<?php

namespace App\Services\User\Impl;

use App\Models\User\Role;
use App\Services\User\RoleService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class RoleServiceImpl implements RoleService
{
    private Role $roleModel;

    public function __construct(Role $role)
    {
        $this->roleModel = $role;
    }

    /**
     * Get Roles
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getRoles(Request $request): Collection|LengthAwarePaginator
    {
        $roles = $this->roleModel->search($request->input('search'));

        if (!isPaginated($request)) return $roles->get();

        return $roles->paginate($request->perPage ?? null);
    }

    /**
     * Get Role
     *
     * @param Request $request
     * @param string|null $param
     * @return Role|null
     */
    public function getRole(Request $request, ?string $param = null): ?Role
    {
        $role = $this->roleModel;

        if ($param) {
            $role = $role->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%");
            });
        }

        return $request->input('find_or_fail') ? $role->firstOrFail() : $role->first();
    }

    /**
     * Create role
     *
     * @param Request $request
     * @return Model|Role
     */
    public function createRole(Request $request): Model|Role
    {
        return $this->roleModel->create(
            array_filter([
                'id' => $request->input('id'),
                'name' => $request->input('name'),
                'guard_name' => $request->input('guard_name'),
            ], customArrayFilter())
        );
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return Role
     */
    public function updateRole(Request $request, Role $role): Role
    {
        $role->update(
            array_filter([
                'name' => $request->input('name'),
                'guard_name' => $request->input('guard_name'),
            ], customArrayFilter())
        );

        return $role->refresh();
    }

    /**
     * Delete Roles
     *
     * @param Request $request
     * @param array $roleIds
     * @return int
     */
    public function deleteRoles(Request $request, array $roleIds): int
    {
        return $this->roleModel->destroy($roleIds);
    }
}
