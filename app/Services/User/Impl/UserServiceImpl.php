<?php

namespace App\Services\User\Impl;

use App\Models\User\User;
use App\Services\User\UserService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;

class UserServiceImpl implements UserService
{
    private User $userModel;

    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    /**
     * Get Users
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getUsers(Request $request): Collection|LengthAwarePaginator
    {
        $users = $this->userModel->search($request->input('search'));

        if (!isPaginated($request)) return $users->get();

        return $users->paginate($request->perPage ?? null);
    }

    /**
     * Get User
     *
     * @param Request $request
     * @param string|null $param
     * @return User|null
     */
    public function getUser(Request $request, ?string $param = null): ?User
    {
        $user = $this->userModel;

        if ($param) {
            $user = $user->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%");
            });
        }

        return $request->input('find_or_fail') ? $user->firstOrFail() : $user->first();
    }

    /**
     * Check User Password Is Correct
     *
     * @param User $user
     * @param string $password
     * @return bool
     */
    public function checkUserPasswordIsCorrect(User $user, string $password): bool
    {
        return Hash::check($password, $user->password);
    }

    /**
     * Create User
     *
     * @param Request $request
     * @return User
     */
    public function createUser(Request $request): User
    {
        return $this->userModel->create(
            array_filter([
                'id' => $request->input('id'),
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'email_verified_at' => $request->input('email_verified_at'),
                'password' => $request->input('password') ? Hash::make($request->input('password')) : null,
            ], customArrayFilter())
        );
    }

    /**
     * Update User
     *
     * @param Request $request
     * @param User $user
     * @return User
     */
    public function updateUser(Request $request, User $user): User
    {
        $user->update(
            array_filter([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'email_verified_at' => $request->input('email_verified_at'),
                'password' => $request->input('password') ? Hash::make($request->input('password')) : null,
            ], customArrayFilter())
        );

        return $user->refresh();
    }

    /**
     * Sync User Roles
     *
     * @param User $user
     * @param array $roleNames
     * @return User
     */
    public function syncUserRoles(User $user, array $roleNames): User
    {
        $user->syncRoles($roleNames);
        return $user;
    }

    /**
     * Delete Users
     *
     * @param Request $request
     * @param array $userIds
     * @return int
     */
    public function deleteUsers(Request $request, array $userIds): int
    {
        return $this->userModel->destroy($userIds);
    }

    /**
     * Delete Assigned Roles
     *
     * @param array $userIds
     * @return Collection $users
     */
    public function deleteAssignedRoles(array $userIds): Collection
    {
        $users = $this->userModel->whereIn('id', $userIds)->get();

        foreach ($users as $user) $user->roles()->detach();

        return $users;
    }
}
