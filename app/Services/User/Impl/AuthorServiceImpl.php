<?php

namespace App\Services\User\Impl;

use App\Models\User\Author;
use App\Services\User\AuthorService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class AuthorServiceImpl implements AuthorService
{
    private Author $authorModel;

    public function __construct(Author $author)
    {
        $this->authorModel = $author;
    }

    /**
     * Get Authors
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getAuthors(Request $request): Collection|LengthAwarePaginator
    {
        $authors = $this->authorModel->search($request->input('search'));

        if (!isPaginated($request)) return $authors->get();

        return $authors->paginate($request->perPage ?? null);
    }

    /**
     * Get Author
     *
     * @param Request $request
     * @param string|null $param
     * @return Author|null
     */
    public function getAuthor(Request $request, ?string $param = null): ?Author
    {
        $author = $this->authorModel;

        if ($param) {
            $author = $author->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%")
                    ->orWhere('slug', '=', $param);
            });
        }

        return $request->input('find_or_fail') ? $author->firstOrFail() : $author->first();
    }

    /**
     * Create Author
     *
     * @param Request $request
     * @return Model|Author
     */
    public function createAuthor(Request $request): Model|Author
    {
        return $this->authorModel->create(
            array_filter([
                'id' => $request->input('id'),
                'user_id' => $request->input('user_id'),
                'slug' => $request->input('slug'),
                'name' => $request->input('name'),
                'origin_name' => $request->input('origin_name'),
                'biography' => $request->input('biography'),
            ], customArrayFilter())
        );
    }

    /**
     * Update Author
     *
     * @param Request $request
     * @param Author $author
     * @return Author
     */
    public function updateAuthor(Request $request, Author $author): Author
    {
        $author->update(
            array_filter([
                'user_id' => $request->input('user_id'),
                'slug' => $request->input('slug'),
                'name' => $request->input('name'),
                'origin_name' => $request->input('origin_name'),
                'biography' => $request->input('biography'),
            ], customArrayFilter())
        );

        return $author->refresh();
    }

    /**
     * Delete Authors
     *
     * @param Request $request
     * @param array $authorIds
     * @return int
     */
    public function deleteAuthors(Request $request, array $authorIds): int
    {
        return $this->authorModel->destroy($authorIds);
    }
}
