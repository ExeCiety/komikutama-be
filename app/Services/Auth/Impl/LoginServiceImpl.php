<?php

namespace App\Services\Auth\Impl;

use App\Models\User\User;
use App\Services\Auth\LoginService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class LoginServiceImpl implements LoginService
{
    private User $userModel;

    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    /**
     * Check And Get User Logged In
     * @param Request $request
     * @return User|Model
     * @throws Exception
     */
    public function checkAndGetUserLoggedIn(Request $request): User|Model
    {
        $emailReq = $request->input('email');
        $passwordReq = $request->input('password');

        $userLoggedIn = $this->userModel->query()->where('email', $emailReq)->first();

        if (!$userLoggedIn) throw new Exception(trans('auth.failed'), Response::HTTP_BAD_REQUEST);

        if (!Hash::check(strval($passwordReq), $userLoggedIn->password)) throw new Exception(trans('auth.failed'), Response::HTTP_BAD_REQUEST);

        return $userLoggedIn;
    }

    /**
     * Create Auth Personal Access Token
     *
     * @param User $user
     * @return string
     */
    public function createAuthPersonalAccessToken(User $user): string
    {
        return $user->createToken(config('passport.token_names.auth'))->accessToken;
    }

    /**
     * Revoke Personal Access Tokens
     *
     * @param Collection $tokens
     * @return bool
     */
    public function revokePersonalAccessTokens(Collection $tokens): bool
    {
        $isSuccess = false;

        try {
            $tokens->each(function ($token) {
                $token->revoke();
            });

            $isSuccess = true;
        } catch (\Throwable $th) {
            report($th);
        }

        return $isSuccess;
    }
}
