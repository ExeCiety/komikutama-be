<?php

namespace App\Services\Auth;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface LoginService
{
    public function checkAndGetUserLoggedIn(Request $request): User|Model;

    public function createAuthPersonalAccessToken(User $user): string;

    public function revokePersonalAccessTokens(Collection $tokens): bool;
}
