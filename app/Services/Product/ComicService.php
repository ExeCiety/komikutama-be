<?php

namespace App\Services\Product;

use App\Models\Product\Comic;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface ComicService
{
    public function getComics(Request $request): Collection|LengthAwarePaginator;

    public function findComic(Request $request, ?string $param = null): ?Comic;

    public function createComic(Request $request): Comic;

    public function updateComic(Request $request, Comic $comic): Comic;

    public function deleteComics(Request $request, array $comicIds): int;
}
