<?php

namespace App\Services\Product;

use App\Models\Product\Genre;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface GenreService
{
    public function getGenres(Request $request): Collection|LengthAwarePaginator;

    public function getGenre(Request $request, ?string $param = null): ?Genre;

    public function createGenre(Request $request): Genre;

    public function updateGenre(Request $request, Genre $genre): Genre;

    public function deleteGenres(Request $request, array $genreIds): int;
}
