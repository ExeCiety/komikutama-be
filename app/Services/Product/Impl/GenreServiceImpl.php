<?php

namespace App\Services\Product\Impl;

use App\Models\Product\Genre;
use App\Services\Product\GenreService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class GenreServiceImpl implements GenreService
{
    private Genre $genreModel;

    public function __construct(Genre $genre)
    {
        $this->genreModel = $genre;
    }

    /**
     * Get Genres
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getGenres(Request $request): Collection|LengthAwarePaginator
    {
        $genres = $this->genreModel->search($request->input('search'));

        if (!isPaginated($request)) return $genres->get();

        return $genres->paginate($request->per_page ?? null);
    }

    /**
     * Get Genre
     *
     * @param Request $request
     * @param string|null $param
     * @return Genre|null
     */
    public function getGenre(Request $request, ?string $param = null): ?Genre
    {
        $genre = $this->genreModel;

        if ($param) {
            $genre = $genre->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%")
                    ->orWhere('slug', '=', $param);
            });
        }

        return $request->input('find_or_fail') ? $genre->firstOrFail() : $genre->first();
    }

    /**
     * Create Genre
     *
     * @param Request $request
     * @return Genre
     */
    public function createGenre(Request $request): Genre
    {
        return $this->genreModel->create(
            array_filter([
                'id' => $request->input('id'),
                'slug' => $request->input('slug'),
                'name' => $request->input('name'),
            ], customArrayFilter())
        );
    }

    /**
     * Update Genre
     *
     * @param Request $request
     * @param Genre $genre
     * @return Genre
     */
    public function updateGenre(Request $request, Genre $genre): Genre
    {
        $genre->update(
            array_filter([
                'slug' => $request->input('slug'),
                'name' => $request->input('name'),
            ], customArrayFilter())
        );

        return $genre->refresh();
    }

    /**
     * Delete Genres
     *
     * @param Request $request
     * @param array $genreIds
     * @return int
     */
    public function deleteGenres(Request $request, array $genreIds): int
    {
        return $this->genreModel->destroy($genreIds);
    }
}
