<?php

namespace App\Services\Product\Impl;

use App\Helpers\Model\Other\FileHelper;
use App\Helpers\Model\Product\ChapterHelper;
use App\Helpers\Model\Product\PageHelper;
use App\Models\Product\Chapter;
use App\Services\Product\ChapterService;
use App\Traits\Storage\FileModelRelation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ChapterServiceImpl implements ChapterService
{
    use FileModelRelation;

    private Chapter $chapterModel;

    public function __construct(Chapter $chapter)
    {
        $this->chapterModel = $chapter;
    }

    /**
     * Get Chapters
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getChapters(Request $request): Collection|LengthAwarePaginator
    {
        $chapters = $this->chapterModel->search($request->input('search'));

        $chapters->when($request->ownerable_type, function ($q) use ($request) {
            $q->where('ownerable_type', $request->ownerable_type);
        });

        $chapters->when($request->ownerable_id, function ($q) use ($request) {
            $q->where('ownerable_id', $request->ownerable_id);
        });

        if (!isPaginated($request)) return $chapters->get();

        return $chapters->paginate($request->per_page ?? null);
    }

    /**
     * Find Chapter
     *
     * @param Request $request
     * @param string|null $param
     * @return Chapter|null
     */
    public function findChapter(Request $request, ?string $param = null): ?Chapter
    {
        $chapter = $this->chapterModel;

        if ($param) {
            $chapter = $chapter->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%")
                    ->orWhere('slug', '=', $param);
            });
        }

        return $request->input('find_or_fail') ? $chapter->firstOrFail() : $chapter->first();
    }

    /**
     * Create Chapter
     *
     * @param Request $request
     * @return Chapter
     * @throws Exception
     */
     public function createChapter(Request $request): Chapter
     {
         $chapter = $this->chapterModel->create(
            array_filter([
                'publisher_id' => $request->input('publisher_id'),
                'ownerable_type' => $request->input('ownerable_type'),
                'ownerable_id' => $request->input('ownerable_id'),
                'title' => $request->input('title'),
                'slug' => $request->input('slug'),
                'sequence' => $request->input(['sequence'])
            ], customArrayFilter())
        );

        foreach ($request->pages as $requestPage) {
            $page = $chapter->pages()->create(
                array_filter([
                    'content' => $requestPage['content'] ?? null,
                    'sequence' => $requestPage['sequence'] ?? null
                ], customArrayFilter())
            );

            if (is_array($requestPage['medias'])) {
                $this->initModelSyncFiles($page, [
                    'file_prefix' => PageHelper::$filePrefix,
                    'file_variants' => FileHelper::getImageVariantNames(PageHelper::$fileVariants),
                    'use_webp' => PageHelper::$useWebp
                ])
                    ->syncModelFiles($request->input('medias'))
                    ->syncMoveModelFiles();
            }
        }

        if (is_array($request->input(['medias']))) {
            $this->initModelSyncFiles($chapter, [
                'file_prefix' => ChapterHelper::$filePrefix,
                'file_variants' => FileHelper::getImageVariantNames(ChapterHelper::$fileVariants),
                'use_webp' => ChapterHelper::$useWebp
            ])
                ->syncModelFiles($request->input('medias'))
                ->syncMoveModelFiles();
        }

         return $chapter;
     }

     /**
      * Update Chapter
      * @param Request $request
      * @param Chapter $chapter
      * @return Chapter
      * @trhows Exception
      */
      public function updateChapter(Request $request, Chapter $chapter): Chapter
      {
        $chapter->update(
            array_filter([
                'publisher_id' => $request->input('publisher_id'),
                'ownerable_type' => $request->input('ownerable_type'),
                'ownerable_id' => $request->input('ownerable_id'),
                'title' => $request->has('title') && $request->input('title') != $chapter->title ? $request->input('title') : null,
                'slug' => $request->has('title') && $request->input('title') != $chapter->title ? $request->input('slug') : null,
                'sequence' => $request->input(['sequence'])
            ], customArrayFilter())
        );

        if (is_array($request->input('pages'))) {
            $chapter->pages()->delete();

            foreach ($request->pages as $requestPage) {
                $page = $chapter->pages()->create(
                    array_filter([
                        'content' => $requestPage['content'] ?? null,
                        'sequence' => $requestPage['sequence'] ?? null
                    ], customArrayFilter())
                );

                if (is_array($requestPage['medias'])) {
                    $this->initModelSyncFiles($page, [
                        'file_prefix' => PageHelper::$filePrefix,
                        'file_variants' => FileHelper::getImageVariantNames(PageHelper::$fileVariants),
                        'use_webp' => PageHelper::$useWebp
                    ])
                        ->syncModelFiles($requestPage['medias'])
                        ->syncMoveModelFiles();
                }
            }
        }

        if (is_array($request->input(['medias']))) {
            $this->initModelSyncFiles($chapter, [
                'file_prefix' => ChapterHelper::$filePrefix,
                'file_variants' => FileHelper::getImageVariantNames(ChapterHelper::$fileVariants),
                'use_webp' => ChapterHelper::$useWebp
            ])
                ->syncModelFiles($request->input('medias'))
                ->syncMoveModelFiles();
        }

        return $chapter->refresh();
      }

      /**
       * Delete Chapter
       *
       * @param Request $request
       * @param array $chapterIds
       * @return int
       */
      public function deleteChapters(Request $request, array $chapterIds): int
      {
          return $this->chapterModel->destroy($chapterIds);
      }
}
