<?php

namespace App\Services\Product\Impl;

use App\Helpers\Model\Other\FileHelper;
use App\Helpers\Model\Product\ComicHelper;
use App\Models\Product\Comic;
use App\Services\Product\ComicService;
use App\Traits\Storage\FileModelRelation;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ComicServiceImpl implements ComicService
{
    use FileModelRelation;

    private Comic $comicModel;

    public function __construct(Comic $comic)
    {
        $this->comicModel = $comic;
    }

    /**
     * Get Comics
     *
     * @param Request $request
     * @return Collection|LengthAwarePaginator
     */
    public function getComics(Request $request): Collection|LengthAwarePaginator
    {
        $comics = $this->comicModel->search($request->input('search'));

        if (!isPaginated($request)) return $comics->get();

        return $comics->paginate($request->per_page ?? null);
    }

    /**
     * Find Comic
     *
     * @param Request $request
     * @param string|null $param
     * @return Comic|null
     */
    public function findComic(Request $request, ?string $param = null): ?Comic
    {
        $comic = $this->comicModel;

        if ($param) {
            $comic = $comic->where(function ($q) use ($param) {
                $q->where('id', 'ILIKE', "%$param%")
                    ->orWhere('slug', '=', $param);
            });
        }

        return $request->input('find_or_fail') ? $comic->firstOrFail() : $comic->first();
    }

    /**
     * Create Comic
     *
     * @param Request $request
     * @return Comic
     * @throws Exception
     */
    public function createComic(Request $request): Comic
    {
        $comic = $this->comicModel->create(
            array_filter([
                'id' => $request->input('id'),
                'publisher_id' => $request->input('publisher_id'),
                'slug' => $request->input('slug'),
                'title' => $request->input('title'),
                'alternative_title' => $request->input('alternative_title'),
                'synopsis' => $request->input('synopsis'),
                'year_released_at' => $request->input('year_released_at'),
                'rank' => $request->input('rank'),
                'rating' => $request->input('rating'),
                'status' => $request->input('status'),
                'type' => $request->input('type'),
            ], customArrayFilter())
        );

        $comic->authors()->sync($request->input('author_id'));
        $comic->artists()->sync($request->input('artist_id'));

        if (is_array($request->input(['medias']))) {
            $this->initModelSyncFiles($comic, [
                'file_prefix' => ComicHelper::$filePrefix,
                'file_variants' => FileHelper::getImageVariantNames(ComicHelper::$fileVariants),
                'use_webp' => ComicHelper::$useWebp
            ])
                ->syncModelFiles($request->input('medias'))
                ->syncMoveModelFiles();
        }

        return $comic;
    }

    /**
     * Update Comic
     *
     * @param Request $request
     * @param Comic $comic
     * @return Comic
     * @throws Exception
     */
    public function updateComic(Request $request, Comic $comic): Comic
    {
        $comic->update(
            array_filter([
                'publisher_id' => $request->input('publisher_id'),
                'slug' => $request->input('slug'),
                'title' => $request->input('title'),
                'alternative_title' => $request->input('alternative_title'),
                'synopsis' => $request->input('synopsis'),
                'year_released_at' => $request->input('year_released_at'),
                'rank' => $request->input('rank'),
                'rating' => $request->input('rating'),
                'status' => $request->input('status'),
                'type' => $request->input('type'),
            ], customArrayFilter())
        );

        if (is_array($request->input('author_id'))) $comic->authors()->sync($request->input('author_id'));
        if (is_array($request->input('artist_id'))) $comic->artists()->sync($request->input('artist_id'));

        if (is_array($request->input(['medias']))) {
            $this->initModelSyncFiles($comic, [
                'file_prefix' => ComicHelper::$filePrefix,
                'file_variants' => FileHelper::getImageVariantNames(ComicHelper::$fileVariants),
                'use_webp' => ComicHelper::$useWebp
            ])
                ->syncModelFiles($request->input('medias'))
                ->syncMoveModelFiles();
        }

        return $comic->refresh();
    }

    /**
     * Delete Comic
     *
     * @param Request $request
     * @param array $comicIds
     * @return int
     */
    public function deleteComics(Request $request, array $comicIds): int
    {
        return $this->comicModel->destroy($comicIds);
    }
}
