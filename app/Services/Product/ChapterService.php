<?php

namespace App\Services\Product;

use App\Models\Product\Chapter;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface ChapterService
{
    public function getChapters(Request $request): Collection|LengthAwarePaginator;

    public function findChapter(Request $request, ?string $param = null): ?Chapter;

    public function createChapter(Request $request): Chapter;

    public function updateChapter(Request $request, Chapter $chapter): Chapter;

    public function deleteChapters(Request $request, array $chapterIds): int;
}
