<?php

use Illuminate\Support\Facades\Storage;

if (!function_exists('resolveTempFileName')) {
    /**
     * Remove prefix "temp" and change the "v1" infix
     *
     * @param string $filename
     * @param string $infix
     * @return array|string
     */
    function resolveTempFileName(string $filename, string $infix = 'v1'): array|string
    {
        return str_replace('temp_', '', str_replace('_v1_', "_{$infix}_", $filename));
    }
}

if (!function_exists('moveFile')) {
    /**
     * Move file to another directory
     *
     * @param array $file
     * @return int
     */
    function moveFile(array $file): int
    {
        $totalFilesMoved = 0;

        $isFileExists = Storage::disk($file['previous_disk'])->exists($file['file']);
        $isWebpFileExists = Storage::disk($file['previous_disk'])->exists("{$file['file']}.webp");

        $isNewFileMissing = Storage::disk($file['disk'])->missing($file['new_file']);
        $isNewWebpFileMissing = Storage::disk($file['disk'])->missing("{$file['new_file']}.webp");

        if ($isNewFileMissing && $isFileExists) {
            if ($file['previous_disk'] == $file['disk']) {
                Storage::disk($file['disk'])->move($file['file'], $file['new_file']);
                $totalFilesMoved++;
                if (isset($file['webp']) && $file['webp'] && $isNewWebpFileMissing && $isWebpFileExists) {
                    Storage::disk($file['disk'])->move("{$file['file']}.webp", "{$file['new_file']}.webp");
                    $totalFilesMoved++;
                }
            } else {
                Storage::disk($file['disk'])->put($file['new_file'], Storage::disk($file['previous_disk'])->get($file['file']));
                $totalFilesMoved++;

                if (isset($file['webp']) && $file['webp'] && $isNewWebpFileMissing && $isWebpFileExists) {
                    Storage::disk($file['disk'])->put("{$file['new_file']}.webp", Storage::disk($file['previous_disk'])->get("{$file['file']}.webp"));
                    $totalFilesMoved++;
                    Storage::disk($file['previous_disk'])->delete("{$file['file']}.webp");
                    $totalFilesMoved++;
                }

                Storage::disk($file['previous_disk'])->delete($file['file']);
                $totalFilesMoved++;
            }
        }

        return $totalFilesMoved;
    }
}

if (!function_exists('deleteFile')) {
    /**
     * Delete file from directory
     *
     * @param string $path
     * @param string $disk
     * @param bool $webp
     * @return int
     */
    function deleteFile(string $path, string $disk = 'public', bool $webp = false): int
    {
        $totalFilesDeleted = 0;

        if (Storage::disk($disk)->exists($path)) {
            if ($webp && Storage::disk($disk)->exists($path . ".webp")) {
                Storage::disk($disk)->delete($path . ".webp");
                $totalFilesDeleted++;
            }

            Storage::disk($disk)->delete($path);
            $totalFilesDeleted++;
        }

        return $totalFilesDeleted;
    }
}
