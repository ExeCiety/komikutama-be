<?php

namespace App\Helpers\Http;

class RouteHelper
{
    public static array $apiRouteVersionPrefixes = [
        'v1' => [
            'name' => 'v1'
        ]
    ];

    public static array $routePrefixes = [
        'admin' => [
            'name' => 'admin'
        ],
        'general' => [
            'name' => 'general'
        ],
        'public' => [
            'name' => 'public'
        ],
    ];

    public static function isRouteFromPrefixes(array $prefixes): bool
    {
        return array_any($prefixes, request()->segments());
    }
}
