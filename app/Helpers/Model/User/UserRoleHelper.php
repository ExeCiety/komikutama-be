<?php

namespace App\Helpers\Model\User;

class UserRoleHelper
{
    public static array $userRoles = [
        'admin' => [
            'name' => 'admin'
        ],
        'creator' => [
            'name' => 'creator'
        ],
        'reader' => [
            'name' => 'reader'
        ]
    ];

    /**
     * Get All Role Names
     *
     * @return array
     */
    public static function getAllRoleNames(): array
    {
        return array_values(
            array_map(function ($role) {
                return $role['name'];
            }, self::$userRoles)
        );
    }
}
