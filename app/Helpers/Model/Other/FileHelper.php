<?php

namespace App\Helpers\Model\Other;

use App\Helpers\Model\Product\ComicHelper;
use App\Models\Other\File;
use App\Models\Product\Comic;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileHelper
{
    public static int $maxUploadSize = 8388608, $maxOptimizeSize = 2097152;

    public static string $publicDiskName = 'public';
    public static string $localDiskName = 'local';
    public static string $s3DiskName = 's3';
    public static string $tempDiskName = 'temp';
    public static string $defaultDiskName = 'public';

    public static string $s3BucketName = 'komikutama';
    public static string $regionName = 'ap-southeast-1';

    public static array $imageVariantNames = [
        'default' => 'default',
        'comic' => 'comic'
    ];
    public static array $imageVariations = [
        [
            'name' => 'default',
            'variants' => [
                [
                    'name' => 'thumbnail',
                    'width' => 150,
                    'height' => 150
                ]
            ]
        ]
    ];

    /**
     * Generate Temp File Name
     *
     * @param string $prefix
     * @param string $extension
     * @return string
     */
    public function generateTempFileName(string $prefix, string $extension): string
    {
        return Str::lower(Str::random(10))
            . '_' . $prefix
            . '_' . now()->addHour()->timestamp
            . $extension;
    }

    /**
     * Generate Temp File Name
     *
     * @param string $filename
     * @param string $infix
     * @return string
     */
    public function resolveTempFileName(string $filename, string $infix = 'v1'): string
    {
        return str_replace('temp_', '', str_replace('v1', $infix, $filename));
    }

    /**
     * Resolve File Url From Disk
     *
     * @param string $file_value
     * @param string $disk_name
     * @return string
     */
    public function resolveFileUrlFromDisk(string $file_value, string $disk_name = 'public'): string
    {
        $regionName = self::$regionName;
        $s3BucketName = self::$s3BucketName;

        if ($disk_name == self::$s3DiskName) {
            $fileUrl = "https://s3.{$regionName}.amazonaws.com/{$s3BucketName}/{$file_value}";
        } else {
            $fileUrl = secure_url("cdn/{$disk_name}/{$file_value}");
        }

        return $fileUrl;
    }

    /**
     * Get Image Variations
     *
     * @param string|null $type
     * @return array|array[]
     */
    public static function getImageVariations(string $type = null): array
    {
        $variations = collect(self::$imageVariations);
        $variation = $variations->filter(function ($v) {
                return $v['name'] === self::$imageVariantNames['default'];
            })->values()->first() ?? [];

        if ($type === Comic::$morphName) {
            $variation = [
                'variants' => ComicHelper::$fileVariants
            ];
        }

        return $variation['variants'] ?? [];
    }

    /**
     * Get Image Variant Names
     *
     * @param array $fileVariants
     * @return array
     */
    public static function getImageVariantNames(array $fileVariants): array
    {
        return array_values(
            array_map(function ($variant) {
                return $variant['name'];
            }, $fileVariants)
        );
    }

    /**
     * Generate Url Value Model File
     *
     * @param File $file
     * @return string
     */
    public static function generateUrlValueModelFile(File $file): string
    {
        if ($file->type === 'youtube_video') return $file->value;

        if (self::$defaultDiskName === self::$s3DiskName) {
            return Storage::disk(self::$defaultDiskName)
                    ->url('/' . config('filesystems.disks.s3.bucket')) . "/{$file->value}";
        }

        return Storage::disk(self::$defaultDiskName)->url($file->value);
    }
}
