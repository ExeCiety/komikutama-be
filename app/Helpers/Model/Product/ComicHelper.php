<?php

namespace App\Helpers\Model\Product;

class ComicHelper
{
    public static array $comicStatuses = [
        'ongoing', 'hiatus', 'completed', 'dropped'
    ];

    public static array $comicTypes = [
        'manhwa', 'manga', 'manhua'
    ];

    public static string $filePrefix = 'comic';

    public static array $fileVariants = [
        [
            'name' => 'thumbnail',
            'width' => 267,
            'height' => 390
        ]
    ];

    public static bool $useWebp = true;
}
