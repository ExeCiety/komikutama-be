<?php

namespace App\Helpers\Model\Product;

class ChapterHelper
{
    public static string $filePrefix = 'chapter';

    public static array $fileVariants = [
        [
            'name' => 'thumbnail',
            'width' => 133,
            'height' => 80
        ]
    ];

    public static bool $useWebp = true;
}
