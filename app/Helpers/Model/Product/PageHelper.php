<?php

namespace App\Helpers\Model\Product;

class PageHelper
{
    public static string $filePrefix = 'page';

    public static array $fileVariants = [];

    public static bool $useWebp = true;
}
