<?php

namespace App\Traits\Model;

use Illuminate\Support\Str;

/**
 * @method static creating(\Closure $param)
 */
trait UuidPrimaryKey
{
    /**
     * Boot functions from Laravel
     */
    protected static function boot(): void
    {
        parent::boot();

        static::creating(function ($model) {
            $model->incrementing = $model->getIncrementing();
            $model->keyType = $model->getKeyType();
            $model->{$model->getKeyName()} = $model->id ?? Str::uuid()->toString();
        });
    }

    public function getIncrementing(): bool
    {
        return false;
    }

    public function getKeyType(): string
    {
        return 'string';
    }
}
