<?php

namespace App\Traits\Storage;

use App\Helpers\Model\Other\FileHelper;
use App\Jobs\File\ConvertWebp;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

trait UploadFile
{
    private $file, $filePrefix;

    protected array $imageExtensions = [
        'jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'webp', 'jfif', 'apng', 'avif', 'pjpeg', 'pjp'
    ];

    /**
     * Upload Request Files To Storage
     *
     * @param Request $request
     * @return array
     */
    public function uploadRequestFilesToStorage(Request $request): array
    {
        if (!$this->request) $this->request = $request;

        $fileHelper = new FileHelper();
        $files = [];

        foreach ($request->file('file') as $file) {
            $this->file = $file;

            $urlFile = Storage::disk(FileHelper::$tempDiskName)
                ->url($this->uploadRequestFileToStorage(
                    $this->file,
                    $fileHelper->generateTempFileName($this->getFilePrefix(), $this->getFileExtension()),
                    FileHelper::$tempDiskName
                ));

            $fileName = pathinfo($urlFile, PATHINFO_BASENAME);
            $fileExtension = pathinfo($urlFile, PATHINFO_EXTENSION);

            $pathInfo = Storage::disk(FileHelper::$tempDiskName)->path($fileName);

            $files[] = [
                'original' => [
                    'filename' => $file->getClientOriginalName()
                ],
                'url' => $urlFile,
                'path' => $pathInfo,
                'filename' => $fileName,
                'extension' => $fileExtension,
                'expired' => now()->addDay()->toDateTimeString()
            ];
        }

        $this->makeFileVariations($files);

        return $files;
    }

    /**
     * Upload File To Storage
     *
     * @param UploadedFile $file
     * @param string $filename
     * @param string $diskName
     * @return string|false
     */
    public function uploadRequestFileToStorage(UploadedFile $file, string $filename, string $diskName): bool|string
    {
        return $file->storeAs('', "temp_{$filename}", $diskName);
    }

    /**
     * Make File Variations
     *
     * @param array $temp_files
     * @return self
     */
    protected function makeFileVariations(array $temp_files): static
    {
        foreach ($temp_files as $file_index => $file) {
            if (in_array($file['extension'], $this->imageExtensions)) {
                $image = Storage::disk(FileHelper::$tempDiskName)->get($file['filename']);
                $this->optimizeImage($image, $file);
                $path = $file['path'];

                foreach (FileHelper::getImageVariations($this->request->input('type')) as $imageCanvas) {
                    $canvas = Image::canvas($imageCanvas['width'], $imageCanvas['height']);
                    $newFile = $this->newFileName($path, $imageCanvas);
                    $image = Image::make($image)
                        ->resize($imageCanvas['width'], $imageCanvas['height'], function ($constraint) {
                            $constraint->aspectRatio();
                        });

                    $canvas->insert($image, 'center');
                    $canvas->save(Storage::disk(FileHelper::$tempDiskName)->path('') . $newFile);

                    Storage::disk(FileHelper::$tempDiskName)->put($newFile, $canvas->stream());

                    $this->convertWebP([
                        'new_name' => $newFile,
                        'file_index' => $file_index
                    ], FileHelper::$tempDiskName);
                }

                $this->convertWebP([
                    'new_name' => $file['filename'],
                    'file_index' => $file_index
                ], FileHelper::$tempDiskName);
            }
        }

        return $this;
    }

    /**
     * Get the file prefix
     *
     * @return string
     */
    protected function getFilePrefix(): string
    {
        return $this->filePrefix ?? request()->segment(1);
    }

    /**
     * Get the original file extension
     *
     * @return string
     */
    protected function getFileExtension(): string
    {
        return '.' . $this->file->getClientOriginalExtension();
    }

    /**
     * Calculate the image optimization
     *
     * @param $file
     * @param array $filepath
     * @return void
     */
    protected function optimizeImage($file, array $filepath = []): void
    {
        if (!empty($filepath) && Storage::disk(FileHelper::$tempDiskName)->exists($filepath['filename'])) {
            $image = Image::make($file);
            $size = $image->filesize();

            if ($size > FileHelper::$maxOptimizeSize) {
                $optimizationValue = floor((FileHelper::$maxOptimizeSize / $size) * 100);
                $image->save($file['path'], $optimizationValue);
            }
        }
    }

    /**
     * Get new filename
     *
     * @param string $filename
     * @param array $canvas
     * @return string
     */
    protected function newFileName(string $filename, array $canvas): string
    {
        $basename = pathinfo($filename, PATHINFO_FILENAME);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        return $basename . '_' . $canvas['name'] . '.' . $extension;
    }

    /**
     * Convert webP job
     *
     * @param array $file
     * @param string|null $disk
     * @param int|null $quality
     * @return void
     */
    protected function convertWebP(array $file = [], string $disk = null, int $quality = null): void
    {
        ConvertWebp::dispatch([
            'new_file' => $file['new_name'],
            'file_index' => $file['file_index'],
            'disk' => $disk,
            'quality' => $quality
        ])->onConnection('sync');
    }
}
