<?php

namespace App\Traits\Storage;

use App\Helpers\Model\Other\FileHelper;
use App\Jobs\File\MoveFile;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

trait FileModelRelation
{
    protected Model $uploadedModel;
    protected string $filePrefix;
    protected array $fileVariants;
    protected bool $useWebp;

    protected mixed $unusedFiles;
    protected Collection $newFiles, $fileNames;

    /**
     * Init Model Sync Files
     *
     * @param Model $model
     * @param array $options
     * @return self
     */
    protected function initModelSyncFiles(Model $model, array $options = []): static
    {
        $this->uploadedModel = $model;
        $this->filePrefix = $options['file_prefix'] ?? "";
        $this->fileVariants = $options['file_variants'] ?? [];
        $this->useWebp = $options['use_webp'] ?? false;

        $this->unusedFiles = collect();
        $this->newFiles = collect();
        $this->fileNames = collect();

        return $this;
    }

    /**
     * Sync Files Of Relation Model
     *
     * @param array $files
     * @return self
     * @throws Exception
     */
    protected function syncModelFiles(array $files = []): static
    {
        if (!$this->uploadedModel->files()) {
            throw new Exception(trans('model.model_doesnt_has_file_relationship'), ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }

        $this->unusedFiles = $this->uploadedModel->files();

        if (empty($files)) {
            $this->deleteFiles($this->unusedFiles->get()->map->id->toArray(), ['force_delete' => true]);
        }

        if (is_array($files)) {
            foreach ($files as $index => $file) {
                //  ORIGINAL FILE
                $filename = $file['type'] == "youtube_video" ? $file['value'] : resolveTempFileName($file['value'], $this->filePrefix);
                $_file = $this->uploadedModel->files()->where('value', $filename)->first();

                if (!$_file) {
                    $_file = $this->uploadedModel->files()->create(array_filter(
                        [
                            'value' => $filename,
                            'name' => $file['name'],
                            'type' => $file['type'] ?? null,
                            'sequence' => ($index + 1)
                        ]
                    ));

                    $this->newFiles->push($file);
                    $this->fileNames->push($file['name']);
                }

                $this->unusedFiles = $this->unusedFiles->where('id', '<>', $_file->id);

                if ($file['type'] == "youtube_video") {
                    $this->fileNames->push($file['name']);
                    continue;
                }

                // VARIANT FILES
                $fileInfo = pathinfo($file['value']);

                foreach ($this->fileVariants as $variant) {
                    $filename = resolveTempFileName(
                        "{$fileInfo['filename']}_{$variant}.{$fileInfo['extension']}",
                        $this->filePrefix
                    );
                    $_file = $this->uploadedModel->files()->where('value', $filename)->first();

                    if (!$_file) {
                        $_file = $this->uploadedModel->files()->create(array_filter(
                            [
                                'value' => $filename,
                                'name' => $file['name'],
                                'type' => $file['type'] ?? null,
                                'variant' => $variant,
                                'sequence' => ($index + 1)
                            ]
                        ));

                        $this->newFiles->push($file);
                    }

                    $this->unusedFiles = $this->unusedFiles->where('id', '<>', $_file->id);
                }
            }
        }

        return $this;
    }

    /**
     * Sync Move Model Files
     *
     * @return self
     * @throws Exception
     */
    protected function syncMoveModelFiles(): static
    {
        if (!$this->uploadedModel->files()) {
            throw new \Exception(trans('model.model_doesnt_has_file_relationship'), ResponseAlias::HTTP_INTERNAL_SERVER_ERROR);
        }

        // MOVE NEW FILES
        foreach ($this->newFiles as $file) {
            if ($file['type'] == "youtube_video") continue;

            // MOVE ORIGINAL FILE
            $this->moveFile(
                array_merge($file, [
                    'prefix' => $this->filePrefix,
                    'disk' => FileHelper::$defaultDiskName,
                    'previous_disk' => FileHelper::$tempDiskName,
                    'use_webp' => $this->useWebp
                ])
            );

            // MOVE VARIANT FILES
            $fileInfo = pathinfo($file['value']);
            foreach ($this->fileVariants as $variant) {
                $filename = "{$fileInfo['filename']}_{$variant}.{$fileInfo['extension']}";

                $this->moveFile(
                    array_merge($file, [
                        'value' => $filename,
                        'prefix' => $this->filePrefix,
                        'disk' => FileHelper::$defaultDiskName,
                        'previous_disk' => FileHelper::$tempDiskName,
                        'use_webp' => $this->useWebp
                    ])
                );
            }
        }

        // DELETE UNUSED FILES
        if ($this->unusedFiles->count()) {
            if (!empty($this->file_names)) $this->unusedFiles = $this->unusedFiles->whereIn('name', $this->file_names);

            $this->deleteFiles($this->unusedFiles->get()->map->id->toArray(), ['force_delete' => true]);
        }

        return $this;
    }

    /**
     * Move File
     *
     * @param array $fileInfo
     * @return void
     */
    protected function moveFile(array $fileInfo = []): void
    {
        if (!empty($fileInfo)) MoveFile::dispatch($fileInfo);
    }

    /**
     * Delete Files
     *
     * @param array $file_ids
     * @param array $options
     * @return int
     */
    protected function deleteFiles(array $file_ids = [], array $options = []): int
    {
        $file_exception = $options['file_exception'] ?? [];
        $force_delete = $options['force_delete'] ?? false;

        $files = $this->uploadedModel->files()
            ->withTrashed()
            ->whereIn('id', $file_ids);

        if (!$force_delete) return $files->delete();

        foreach ($files->get() as $file) {
            if (!in_array($file->value, $file_exception) && $file->type != "youtube_video") {
                deleteFile($file->value, FileHelper::$defaultDiskName, $this->useWebp && $file->type == "image");
            }
        }

        return $files->forceDelete();
    }
}
