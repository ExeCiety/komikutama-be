<?php

namespace App\Models\Other;

use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @mixin Builder
 *
 * @property string $value
 * @property string $type
 */
class File extends Model
{
    use HasFactory, UuidPrimaryKey, SoftDeletes;

    public static string $morphName = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'fileable_type',
        'fileable_id',
        'name',
        'value',
        'variant',
        'sequence',
        'type',
    ];

    // Relationships
    // Morph To
    /**
     * Get Model Fillable
     *
     * @return MorphTo
     */
    public function fileable(): MorphTo
    {
        return $this->morphTo();
    }
}
