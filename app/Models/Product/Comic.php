<?php

namespace App\Models\Product;

use App\Models\Other\File;
use App\Models\User\Artist;
use App\Models\User\Author;
use App\Models\User\User;
use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use JetBrains\PhpStorm\ArrayShape;
use Laravel\Scout\Attributes\SearchUsingFullText;
use Laravel\Scout\Searchable;

/**
 *
 * @mixin Builder
 *
 * @property string $id
 * @property string $slug
 * @property string $title
 * @property string $alternative_title
 * @property string $synopsis
 * @property string $year_released_at
 * @property string $status
 * @property string $type
 */
class Comic extends Model
{
    use HasFactory, UuidPrimaryKey, Searchable;

    public static string $morphName = 'comics';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'publisher_id',
        'slug',
        'title',
        'alternative_title',
        'synopsis',
        'year_released_at',
        'rank',
        'rating',
        'status',
        'type',
    ];

    /**
     * Get the index able data array for the model.
     *
     * @return array
     */
    #[
        ArrayShape([
            'id' => "string", 'slug' => "string", 'title' => "string", 'alternative_title' => "string",
            'synopsis' => "string", 'year_released_at' => "string", 'status' => "string", 'type' => "string"
        ])
    ]
    #[SearchUsingFullText(['synopsis'])]
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'alternative_title' => $this->alternative_title,
            'synopsis' => $this->synopsis,
            'year_released_at' => $this->year_released_at,
            'status' => $this->status,
            'type' => $this->year_released_at,
        ];
    }

    // Relationships
    // Belongs to
    /**
     * Relations to Publisher
     *
     * @return BelongsTo
     */
    public function publisher(): BelongsTo
    {
        return $this->belongsTo(User::class, 'publisher_id');
    }

    // Morph Many

    /**
     * Relations to Chapters
     *
     * @return MorphMany
     */
    public function chapters(): MorphMany
    {
        return $this->morphMany(Chapter::class, 'ownerable');
    }

    /**
     * Relations to Files
     *
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'fileable')
            ->orderBy('sequence')
            ->orderBy('created_at');
    }

    // Morph to Many

    /**
     * Relations to Genres
     *
     * @return MorphToMany
     */
    public function genres(): MorphToMany
    {
        return $this->morphToMany(Genre::class, 'genreable');
    }

    /**
     * Relations to Author
     *
     * @return MorphToMany
     */
    public function authors(): MorphToMany
    {
        return $this->morphToMany(Author::class, 'authorable');
    }

    /**
     * Relations to Artist
     *
     * @return MorphToMany
     */
    public function artists(): MorphToMany
    {
        return $this->morphToMany(Artist::class, 'artistable');
    }
}
