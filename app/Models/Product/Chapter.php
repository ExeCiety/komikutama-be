<?php

namespace App\Models\Product;

use App\Models\Other\File;
use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use JetBrains\PhpStorm\ArrayShape;
use Laravel\Scout\Attributes\SearchUsingFullText;
use Laravel\Scout\Searchable;

/**
 * @mixin Builder
 *
 * @property string $id
 * @property string $publisher_id
 * @property string $ownerable_id
 * @property string $ownerable_type
 * @property string $slug
 * @property string $title
 * $property int $sequence
 */
class Chapter extends Model
{
    use HasFactory, UuidPrimaryKey, Searchable;

    public static string $morphName = 'chapters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'publisher_id',
        'ownerable_type',
        'ownerable_id',
        'slug',
        'title',
        'sequence'
    ];

    /**
     * Get the index able data array for the model.
     *
     * @return array
     */
    #[
        ArrayShape([
            'id' => 'string',
            'publisher_id' => 'string',
            'ownerable_type' => 'string',
            'ownerable_id' => 'string',
            'slug' => 'string',
            'title' => 'string',
            'sequence' => 'int',
        ])
    ]
    #[SearchUsingFullText(['title'])]
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'publisher_id' => $this->publisher_id,
            'ownerable_type' => $this->ownerable_type,
            'ownerable_id' => $this->ownerable_id,
            'slug' => $this->slug,
            'title' => $this->title,
            'sequence' => $this->sequence,
        ];
    }

    // Relationships
    // Belongs to
    /**
     * Relation to Publisher
     *
     * @return BelongsTo
     */
    public function publisher(): BelongsTo
    {
        return $this->belongsTo(User::class, 'publisher_id');
    }

    // Morph to
    /**
     * Relation to Ownerable
     *
     * @return MorphTo
     */
    public function ownerable(): MorphTo
    {
        return $this->morphTo();
    }

    // Morph Many
    /**
     * Relation to Pages
     *
     * @return HasMany
     */
    public function pages(): HasMany
    {
        return $this->hasMany(Page::class, 'chapter_id')
            ->orderBy('sequence')
            ->orderBy('created_at');
    }

    /**
     * Relation to files
     *
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'fileable')
            ->orderBy('sequence')
            ->orderBy('created_at');
    }
}
