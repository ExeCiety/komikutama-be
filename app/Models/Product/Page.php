<?php

namespace App\Models\Product;

use App\Models\Other\File;
use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @mixin Builder
 *
 * @property string $id
 * @property string $chapter_id
 * @property string $content
 * @property int $sequence
 */
class Page extends Model
{
    use HasFactory, UuidPrimaryKey;

    public static string $morphName = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'chapter_id',
        'content',
        'sequence'
    ];

    // Relationships
    // Belongs to
    /**
     * Relation to Chapter
     *
     * @return BelongsTo
     */
    public function chapter(): BelongsTo
    {
        return $this->belongsTo(Chapter::class);
    }

    // Morph many
    /**
     * Relation to files
     *
     * @return MorphMany
     */
    public function files(): MorphMany
    {
        return $this->morphMany(File::class, 'fileable')
            ->orderBy('sequence')
            ->orderBy('created_at');
    }
}
