<?php

namespace App\Models\Product;

use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use JetBrains\PhpStorm\ArrayShape;
use Laravel\Scout\Searchable;

/**
 *
 * @mixin Builder
 *
 * @property string $id
 * @property string $slug
 * @property string $name
 */
class Genre extends Model
{
    use HasFactory, UuidPrimaryKey, Searchable;

    public static string $morphName = 'genres';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'slug',
        'name',
    ];

    #[ArrayShape(['id' => "string", 'slug' => "string", 'name' => "string"])]
    public function toSearchableArray(): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name
        ];
    }

    // Relationships
    // Morphed by Many
    /**
     * Relations to Comic
     *
     * @return MorphToMany
     */
    public function comics(): MorphToMany
    {
        return $this->morphedByMany(Comic::class, 'genreable');
    }
}
