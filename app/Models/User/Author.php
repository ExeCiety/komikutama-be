<?php

namespace App\Models\User;

use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JetBrains\PhpStorm\ArrayShape;
use Laravel\Scout\Searchable;

/**
 * @mixin Builder
 *
 * @property string $user_id
 * @property string $slug
 * @property string $name
 * @property string $origin_name
 * @property string $biography
 */
class Author extends Model
{
    use HasFactory, UuidPrimaryKey, Searchable;

    public static string $morphName = 'authors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'user_id',
        'slug',
        'name',
        'origin_name',
        'biography'
    ];

    /**
     * Get the index able data array for the model.
     *
     * @return array
     */
    #[ArrayShape(['user_id' => "string", 'slug' => "string", 'name' => "string", 'origin_name' => "string", 'biography' => "string"])]
    public function toSearchableArray(): array
    {
        return [
            'user_id' => $this->user_id,
            'slug' => $this->slug,
            'name' => $this->name,
            'origin_name' => $this->origin_name,
            'biography' => $this->biography
        ];
    }
}
