<?php

namespace App\Models\User;

use App\Traits\Model\UuidPrimaryKey;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Scout\Searchable;
use Spatie\Permission\Models\Role as SpatieRole;

/**
 * @mixin Builder
 * @property string $name
 * @property string $guard_name
 */
class Role extends SpatieRole
{
    use UuidPrimaryKey, Searchable;

    public static string $morphName = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'name',
        'guard_name',
    ];
}
