<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('comics', function (Blueprint $table) {
            $table->uuid('id')->primary()->default(DB::raw("uuid_generate_v4()"));
            $table->uuid('publisher_id')->nullable();
            $table->string('slug')->unique();
            $table->string('title');
            $table->string('alternative_title');
            $table->text('synopsis');
            $table->year('year_released_at');
            $table->integer('rank')->default(0);
            $table->double('rating')->default(0);
            $table->string('status');
            $table->string('type');

            $table->foreign('publisher_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('comics');
    }
};
