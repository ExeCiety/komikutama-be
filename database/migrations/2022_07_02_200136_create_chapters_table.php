<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->uuid('id')->primary()->default(DB::raw("uuid_generate_v4()"));
            $table->uuid('publisher_id')->nullable();
            $table->uuidMorphs('ownerable');
            $table->string('slug')->unique();
            $table->string('title');
            $table->integer('sequence');

            $table->foreign('publisher_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('chapters');
    }
};
