<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->uuid('id')->primary()->default(DB::raw("uuid_generate_v4()"));
            $table->nullableUuidMorphs('fileable');
            $table->string('name')->nullable();
            $table->text('value');
            $table->string('variant')->nullable()->default('original');
            $table->integer('sequence')->default(1);
            $table->enum('type', ['file', 'image', 'youtube_video'])->default('image');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
