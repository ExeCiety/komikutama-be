<?php

namespace Database\Seeders\User;

use App\Helpers\Model\User\UserRoleHelper;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AddUsersForStartingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $carbonNow = Carbon::now();

        // Admin
        $adminUser = (new User())->create([
            'name' => 'Admin 01',
            'email' => 'admin01@komikutama.com',
            'password' => Hash::make('admin01'),
            'email_verified_at' => $carbonNow->format('Y-m-d H:i:s')
        ]);

        $adminUser->assignRole([UserRoleHelper::$userRoles['admin']['name']]);

        // Creator
        $creatorUser = (new User())->create([
            'name' => 'Creator 01',
            'email' => 'creator01@komikutama.com',
            'password' => Hash::make('creator01'),
            'email_verified_at' => $carbonNow->format('Y-m-d H:i:s')
        ]);

        $creatorUser->assignRole([UserRoleHelper::$userRoles['creator']['name']]);

        // Reader
        $creatorUser = (new User())->create([
            'name' => 'Reader 01',
            'email' => 'reader01@komikutama.com',
            'password' => Hash::make('reader01'),
            'email_verified_at' => $carbonNow->format('Y-m-d H:i:s')
        ]);

        $creatorUser->assignRole([UserRoleHelper::$userRoles['reader']['name']]);
    }
}
