<?php

namespace Database\Seeders\User;

use App\Models\User\Role;
use Illuminate\Database\Seeder;

class AddRolesForStartingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Role::query()->create([
            'name' => 'admin'
        ]);

        Role::query()->create([
            'name' => 'creator'
        ]);

        Role::query()->create([
            'name' => 'reader'
        ]);
    }
}
