<?php

namespace Database\Seeders\User;

use App\Helpers\Model\ModelHelper;
use App\Models\User\Artist;
use App\Models\User\Author;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AddAuthorsAndArtistsForStartingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $authorName = 'Great H';
        Author::query()->create([
            'id' => 'f68c451a-6e01-46a2-bda7-21cf8be6a3ef',
            'slug' => Str::slug($authorName . '-' . Str::random(ModelHelper::$defaultSlugLength)),
            'name' => $authorName,
            'origin_name' => '현절무'
        ]);

        $authorName = 'Hanjung Wolya';
        Author::query()->create([
            'id' => 'a3eebed5-3f9f-40de-8c59-2e82074d22d4',
            'slug' => Str::slug($authorName . '-' . Str::random(ModelHelper::$defaultSlugLength)),
            'name' => $authorName,
            'origin_name' => '한중월야'
        ]);

        $artistName = 'GGBG';
        Artist::query()->create([
            'id' => '5baf1c6e-c5c8-435a-9127-7ff89d4c3164',
            'slug' => Str::slug($artistName . '-' . Str::random(ModelHelper::$defaultSlugLength)),
            'name' => $artistName,
            'origin_name' => '금강불괴'
        ]);
    }
}
