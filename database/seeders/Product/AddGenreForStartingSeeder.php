<?php

namespace Database\Seeders\Product;

use App\Helpers\Model\ModelHelper;
use App\Models\Product\Genre;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AddGenreForStartingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $genres = collect([
            [
                'id' => '1deb8d73-bba6-4108-bbe3-a9fb0728a111',
                'name' => 'Action'
            ],
            [
                'id' => '94be7703-ba0a-451a-9ec1-0f7090fe9e9a',
                'name' => 'Drama'
            ],
            [
                'id' => 'ac510b4c-ddf6-4ea9-a468-f965018a9abb',
                'name' => 'Romance'
            ],
            [
                'id' => '614ebe1f-3924-44b8-a33e-79af8d7377c5',
                'name' => 'Adventure'
            ],
            [
                'id' => '89e4317e-5e9d-41b1-8f1c-35e8821349e7',
                'name' => 'Comedy'
            ],
            [
                'id' => '00a5972c-e007-44d8-9935-13ab3f99b1ca',
                'name' => 'Mistery'
            ],
            [
                'id' => 'c3400422-ce01-4f36-b3bc-ada59a784a8e',
                'name' => 'Fantasy'
            ],
            [
                'id' => '9bec4178-3460-468e-b38e-58b7f8cbc4f3',
                'name' => 'Sci-fi'
            ],
            [
                'id' => '732b354c-af53-4e8d-9b09-c04152d9b769',
                'name' => 'Supernatural'
            ],
            [
                'id' => 'cac289ca-0d4a-4961-9af5-0c8a01da428a',
                'name' => 'Isekai'
            ]
        ]);

        foreach ($genres as $genre) {
            Genre::query()->create([
                'id' => $genre['id'],
                'slug' => Str::slug($genre['name'] . '-' . Str::random(ModelHelper::$defaultSlugLength)),
                'name' => $genre['name']
            ]);
        }
    }
}
