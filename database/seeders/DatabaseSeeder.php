<?php

namespace Database\Seeders;

use Database\Seeders\Product\AddGenreForStartingSeeder;
use Database\Seeders\User\AddAuthorsAndArtistsForStartingSeeder;
use Database\Seeders\User\AddRolesForStartingSeeder;
use Database\Seeders\User\AddUsersForStartingSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call([
            // User
            AddRolesForStartingSeeder::class,
            AddUsersForStartingSeeder::class,
            AddAuthorsAndArtistsForStartingSeeder::class,

            // Product
            AddGenreForStartingSeeder::class,
        ]);
    }
}
