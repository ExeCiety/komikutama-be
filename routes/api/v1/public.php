<?php

use App\Http\Controllers\Api\Public\Auth\LoginController;
use App\Http\Controllers\Api\Public\Product\GenreController;
use App\Http\Controllers\Api\Public\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Public Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [PublicController::class, 'home']);

// Auth
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [LoginController::class, 'login']);
});

// Product
// Genre
Route::group(['prefix' => 'genres'], function () {
    Route::get('/', [GenreController::class, 'index']);
    Route::get('/{param}', [GenreController::class, 'show']);
});
