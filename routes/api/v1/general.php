<?php

use App\Http\Controllers\Api\General\Auth\LoginController;
use App\Http\Controllers\Api\General\GeneralController;
use App\Http\Controllers\Api\General\Other\FileController;
use App\Http\Controllers\Api\General\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API General Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [GeneralController::class, 'home']);

// Auth
Route::group(['prefix' => 'auth'], function () {
    Route::post('/logout', [LoginController::class, 'logout']);
});

// User
// User
Route::group(['prefix' => 'users'], function () {
    Route::get('/self', [UserController::class, 'getUserLoggedIn']);
    Route::put('/self', [UserController::class, 'updateUserLoggedIn']);
    Route::put('/self/change-password', [UserController::class, 'changePasswordUserLoggedIn']);
});

// Other
// File
Route::group(['prefix' => 'files'], function () {
    Route::post('/upload', [FileController::class, 'upload']);
});
