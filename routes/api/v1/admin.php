<?php

use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\Product\ChapterController;
use App\Http\Controllers\Api\Admin\Product\ComicController;
use App\Http\Controllers\Api\Admin\Product\GenreController;
use App\Http\Controllers\Api\Admin\User\ArtistController;
use App\Http\Controllers\Api\Admin\User\AuthorController;
use App\Http\Controllers\Api\Admin\User\RoleController;
use App\Http\Controllers\Api\Admin\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [AdminController::class, 'home']);

// Product
// Comic
Route::group(['prefix' => 'comics'], function () {
    Route::get('/', [ComicController::class, 'index']);
    Route::get('/{param}', [ComicController::class, 'show']);
    Route::post('/', [ComicController::class, 'store']);
    Route::put('/{param}', [ComicController::class, 'update']);
    Route::delete('/', [ComicController::class, 'destroy']);
});

// Genre
Route::group(['prefix' => 'genres'], function () {
    Route::get('/', [GenreController::class, 'index']);
    Route::get('/{param}', [GenreController::class, 'show']);
    Route::post('/', [GenreController::class, 'store']);
    Route::put('/{param}', [GenreController::class, 'update']);
    Route::delete('/', [GenreController::class, 'destroy']);
});

// User
// User
Route::group(['prefix' => 'users'], function () {
    Route::get('/', [UserController::class, 'index']);
    Route::get('/{param}', [UserController::class, 'show']);
    Route::post('/', [UserController::class, 'store']);
    Route::put('/{param}', [UserController::class, 'update']);
    Route::delete('/', [UserController::class, 'destroy']);
});

// Role
Route::group(['prefix' => 'roles'], function () {
    Route::get('/', [RoleController::class, 'index']);
    Route::get('/{param}', [RoleController::class, 'show']);
    Route::post('/', [RoleController::class, 'store']);
    Route::put('/{param}', [RoleController::class, 'update']);
    Route::delete('/', [RoleController::class, 'destroy']);
});

// Author
Route::group(['prefix' => 'authors'], function () {
    Route::get('/', [AuthorController::class, 'index']);
    Route::get('/{param}', [AuthorController::class, 'show']);
    Route::post('/', [AuthorController::class, 'store']);
    Route::put('/{param}', [AuthorController::class, 'update']);
    Route::delete('/', [AuthorController::class, 'destroy']);
});

// Artists
Route::group(['prefix' => 'artists'], function () {
    Route::get('/', [ArtistController::class, 'index']);
    Route::get('/{param}', [ArtistController::class, 'show']);
    Route::post('/', [ArtistController::class, 'store']);
    Route::put('/{param}', [ArtistController::class, 'update']);
    Route::delete('/', [ArtistController::class, 'destroy']);
});

// Chapter
Route::group(['prefix' => 'chapters'], function () {
    Route::get('/', [ChapterController::class, 'index']);
    Route::get('/{param}', [ChapterController::class, 'show']);
    Route::post('/', [ChapterController::class, 'store']);
    Route::put('/{param}', [ChapterController::class, 'update']);
    Route::delete('/', [ChapterController::class, 'destroy']);
});
